
package com.lihua.mytweetdemo.persistance;


import com.lihua.mytweetdemo.business.properties.DatabaseProperty;
import com.lihua.mytweetdemo.business.properties.DatabasePropertyManager;
import com.lihua.mytweetdemo.business.timeline.TimeLineBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  This class implements TimeLineDao to 
 *  define database CRUD
 * @author Lihua
 */
public class TimeLineDaoImpl implements TimeLineDao{

    private static final Logger LOG = LoggerFactory.getLogger(TimeLineDaoImpl.class);
    private static final int ALREADAYSAVED = -1;
    DatabaseProperty database;
    DatabasePropertyManager dataManager ;
    private final  String url ;
    private final String user ;
    private final  String password;

    public TimeLineDaoImpl() throws IOException {
        super();
        database = new DatabaseProperty();
        dataManager = new DatabasePropertyManager();
        user= dataManager.getUser(database, "src/main/resources/", "databaseConfig"); 
        password = dataManager.getPassword(database, "src/main/resources/", "databaseConfig"); 
        url = dataManager.getUrl(database, "src/main/resources/", "databaseConfig"); 
    }
   
    /**
     * This method is find all items in the database
     * and return a list of TimeLineBean
     * @return a list of TimeLineBean
     * @throws SQLException 
     */
    @Override
    public List<TimeLineBean> findAll() throws SQLException {
        List<TimeLineBean> rows = new ArrayList();
        
        String selectQuery = "SELECT * FROM TIMELINETEST";
              
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                TimeLineBean timeLine = makeTimeLine(resultSet);
          
                rows.add(timeLine);
            }
        } catch (SQLException ex) {
            LOG.debug("findAll error", ex.getMessage());
            throw ex;
        }
        return rows;
    }
    
    /**
     * This method is to rebuilt object TimeLineBean use
     * the data retrieved from database
     * @param rs
     * @return
     * @throws SQLException 
     */
    private TimeLineBean makeTimeLine(ResultSet rs) throws SQLException {
        TimeLineBean timeLine = new TimeLineBean();
        timeLine.setTweetId(rs.getLong("TWEETID"));
        timeLine.setName(rs.getString("USERNAME"));
        timeLine.setScreenName(rs.getString("SCREENNAME"));
        timeLine.setImageURL(rs.getString("IMAGE"));
        timeLine.setSource(rs.getString("URL"));
        timeLine.setText(rs.getString("TEXT"));
        timeLine.setCreateAt(rs.getTimestamp("CREATETIME"));
        timeLine.setRetweetCount(rs.getInt("RETWEETCOUNT"));
        timeLine.setIsFavorite(rs.getBoolean("ISFAVORITE"));
        timeLine.setFavoriteCount(rs.getInt("FAVORITECOUNT"));
        
        return timeLine;
    }
    
    /**
     * This is method is used to check if the tweet user has clicked to save 
     * the tweet in database, it return a Boolean true if the tweet
     * is already saved.
     * @param tweetId
     * @return Boolean true: already saved in database, false: haven't saved in database
     * @throws SQLException 
     */
    public boolean isAlreadySaved(long tweetId) throws SQLException{
      
        int result = 0; 
        
        String query = "SELECT COUNT(*) FROM TIMELINETEST WHERE TWEETID = ?";
        try(Connection connection = DriverManager.getConnection(url,user,password);
                PreparedStatement ps = connection.prepareStatement(query);
                )
        {
          
            ps.setLong(1,tweetId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
                }
        }
         catch(SQLException ex)
            {
                LOG.debug("cannot get count in alreadySaved", ex.getMessage());
            }
          
            System.out.println("result"+result);
        
        return result>=1;
    }
    
    /**
     * This method is to insert a tweet into database
     * the method will first check if the tweet is already
     * existed in database or not then do the insertion
     * @param timeLine
     * @return
     * @throws SQLException 
     */
    @Override
    public int create(TimeLineBean timeLine) throws SQLException {
           int result = 0;
                
        if(isAlreadySaved(timeLine.getTweetId()))
        {
            return result = ALREADAYSAVED;
        }
            String createQuery = "INSERT INTO TIMELINETEST (TWEETID, USERNAME,SCREENNAME,IMAGE,URL,TEXT,"
                + "CREATETIME,ISTWEETEDBYME,RETWEETCOUNT,ISFAVORITE,FAVORITECOUNT) VALUES (?,?,?,?,?,?,?,?,?,?,?)";         
       
        try (Connection connection = DriverManager.getConnection(url, user,
                password);
            PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) 
        {
            
            ps.setLong(1, timeLine.getTweetId());
            System.out.println("tweetid: "+timeLine.getTweetId());
            ps.setString(2, timeLine.getName());
            System.out.println("username"+timeLine.getName());
            ps.setString(3, timeLine.getScreenName());
            System.out.println("screenname: "+timeLine.getScreenName());
            ps.setString(4, timeLine.getImageURL());
            System.out.println("Image: "+timeLine.getImageURL());
            ps.setString(5, timeLine.getSource());
            System.out.println("url: "+timeLine.getSource());
            ps.setString(6, timeLine.getText());
            System.out.println("text: "+timeLine.getText());
            ps.setTimestamp(7, timeLine.getCreateAt());
            System.out.println("time: "+timeLine.getCreateAt());
            ps.setBoolean(8,timeLine.isRetweetedByMe());
            System.out.println("isretweetbyme: "+timeLine.isRetweetedByMe());
            ps.setInt(9, timeLine.getRetweetCount());
          
            System.out.println("retweetcount: "+timeLine.getRetweetCount());
            ps.setBoolean(10, timeLine.isFavorited());
              System.out.println("isFavorited"+timeLine.isFavorited());
            ps.setInt(11, timeLine.getFavoriteCount());
           System.out.println("getFavoriteCount"+timeLine.getFavoriteCount());

            result = ps.executeUpdate();
            // Retrieve generated primary key value and assign to bean
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                    System.out.println("recordNum: "+recordNum);
                }
                timeLine.setId(recordNum);
                LOG.debug("New record ID is " + recordNum);
            }
            catch(SQLException ex)
            {
                LOG.debug("cannot create in dao", ex.getMessage());
            }
        }
        catch(SQLException ex){
            LOG.debug("cannot connect to db",ex.getMessage());
        }
        LOG.info("# of records created : " + result);
        
        return result;
    }
    
    
    /**
     * This method is use id to find record in database
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public TimeLineBean findID(int id) throws SQLException {
         int result = 0;
        
         TimeLineBean timeline = new TimeLineBean();
            String selectQuery = "SELECT * FROM TIMELINETEST WHERE ID = ?";         
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user,
                password);
           
            PreparedStatement ps = connection.prepareStatement(selectQuery);) 
        {
            
            ps.setInt(1, id);
       

            
               try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    timeline = makeTimeLine(resultSet);
                }
               }
        }
        
        LOG.info("found timeline: " +id +"result "+result);
   
        
        return timeline;
    }


    /**
     * This method is to delete the saved tweet in database by tweetId
     * @param tweetid
     * @return
     * @throws SQLException 
     */
    @Override
    public int deleteTimeLine(long tweetid) throws SQLException {
            int result;
        String deleteQuery = "DELETE FROM TIMELINETEST WHERE TWEETID = ?";
            

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                PreparedStatement ps = connection.prepareStatement(deleteQuery, Statement.RETURN_GENERATED_KEYS);) 
        {
                ps.setLong(1, tweetid);
                result = ps.executeUpdate();
                 try (ResultSet rs = ps.getGeneratedKeys();) 
                 {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
               
                        LOG.debug("New record ID is " + recordNum);
                }
            }
                
            catch(SQLException ex)
            {
                LOG.debug("cannot delete in dao", ex.getMessage());
            }
        }
        return result;
    
    
}

    @Override
    public int update(TimeLineBean timeLine) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
