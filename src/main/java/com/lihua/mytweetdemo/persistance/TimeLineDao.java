
package com.lihua.mytweetdemo.persistance;

import com.lihua.mytweetdemo.business.timeline.TimeLineBean;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lihua
 */
public interface TimeLineDao {
        // Create

    /**
     *
     * @param timeLine
     * @return
     * @throws SQLException
     */
    public int create(TimeLineBean timeLine) throws SQLException;



    // Read
    public List<TimeLineBean> findAll() throws SQLException;

    public TimeLineBean findID(int id) throws SQLException;
    // Update

    public int update(TimeLineBean timeLine) throws SQLException;

 

    // Delete
    public int deleteTimeLine(long ID) throws SQLException;

}
