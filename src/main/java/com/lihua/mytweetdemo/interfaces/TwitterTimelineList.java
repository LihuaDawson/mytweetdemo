/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.interfaces;

import javafx.scene.Node;

/**
 *
 * @author Lihua
 */
public interface TwitterTimelineList {
    
    public Node getVBoxView();
    public void handleShowTimeline();
    
}
