/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.interfaces;

import com.lihua.mytweetdemo.business.TwitterEngine;
import twitter4j.DirectMessageList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;


/**
 *
 * @author Lihua
 */
public interface TwitterUserInfo {
    public TwitterEngine twitterEngine = new TwitterEngine();
    
    public Twitter twitter = twitterEngine.getTwitterinstance();
    
    public String getUserName();
   
    public long getUserId();
    
    public String getUserImageUrl();
    
    public String getUserScreenName();
    
    public Status getUserStatus();
    
    public String getUserEmail();
    
    public DirectMessageList getMessages()throws TwitterException;
    
    
    
}
