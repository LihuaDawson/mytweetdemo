/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.interfaces;

import com.lihua.mytweetdemo.business.TwitterEngine;
import java.sql.Timestamp;
import java.util.Date;
import twitter4j.Twitter;

/**
 *
 * @author Lihua
 */
public interface TwitterTimeLineInfo {

    final TwitterEngine twitterEngine = new TwitterEngine();
    final Twitter twitter = twitterEngine.getTwitterinstance();

    public String getName();

    public void setName(String name);

    public String getText();

    public void setText(String text);

    public String getImageURL();

    public void setImageURL(String url);

    public String getScreenName();

    public void setScreenName(String screenname);

    public int getFavoriteCount();

    public void setFavoriteCount(int favoriCount);

    public boolean isFavorited();

    public void setFavorited(boolean isFavorite);

    public Date getCreateAt();

    public void setCreateAt(Timestamp createAt);

    public int getRetweetCount();

    public void setRetweetCount(int retweetCount);

    public String getSource();

    public void setSource(String source);

    public long getTweetId();

    public void setTweetId(long tweetid);

    public boolean isRetweetedByMe();

    public void setRetweetedByMe(boolean isRetweetedByMe);

}
