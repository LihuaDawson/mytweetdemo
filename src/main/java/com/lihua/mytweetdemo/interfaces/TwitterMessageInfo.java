/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.interfaces;

import com.lihua.mytweetdemo.business.TwitterEngine;
import java.util.Date;
import twitter4j.Twitter;

/**
 *
 * @author Lihua
 */
public interface TwitterMessageInfo {
    
      final TwitterEngine twitterEngine = new TwitterEngine();
      final Twitter twitter=twitterEngine.getTwitterinstance();
    
      public long getMessageUserId();
      
      public String getMessageUserImageUrl();
      
      public String getMessageUserName();
      
      public Date getMessageDate();
      
      public long getMessageId();
      
      public long getMessageRecipient();
      
      public long getMessageSenderId();
      
      public String getMessageText();
}
