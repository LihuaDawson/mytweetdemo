
package com.lihua.mytweetdemo.business.notification;

import com.lihua.mytweetdemo.business.timeline.TimeLine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import static com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo.twitter;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * This class is used to find all the tweets about me
 * @author Lihua
 */
public class AllAboutMeTask implements TwitterTimelineTask {

    private final static Logger LOG = LoggerFactory.getLogger(MentionTask.class);

    private final ObservableList<TwitterTimeLineInfo> list;

    public AllAboutMeTask(ObservableList<TwitterTimeLineInfo> list) {
        super();
        this.list = list;

    }

    @Override
    public void fillTimeLine() throws Exception {
        List<Status> mentionTweets = twitter.getMentionsTimeline();
        mentionTweets.forEach((result)
                -> {
            list.add(list.size(), new TimeLine(result));

        });
    }

}
