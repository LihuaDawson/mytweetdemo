
package com.lihua.mytweetdemo.business.notification;


import com.lihua.mytweetdemo.business.timeline.TimelineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class is used to generate a list of tweets @ me
 * @author Lihua
 */
public class MentionList extends TimelineList {

    private final static Logger LOG = LoggerFactory.getLogger(MentionList.class);
    private final ListView<TwitterTimeLineInfo> listView;
    private TwitterTimelineTask task;

    public MentionList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception {
        super(listView);
        this.listView = listView;
    }

    @Override
    public void handleShowTimeline() {
        if (task == null) {
            task = new MentionTask(listView.getItems());
        }
        try {

            task.fillTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }
    }

}
