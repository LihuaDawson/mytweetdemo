/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.notification;

import com.lihua.mytweetdemo.business.timeline.TimelineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class is used to find the list of all tweets about me
 * @author Lihua
 */
public class AllAboutMeList extends TimelineList {

    private final static Logger LOG = LoggerFactory.getLogger(AllAboutMeList.class);
    private final ListView<TwitterTimeLineInfo> listView;
    private TwitterTimelineTask task;

    public AllAboutMeList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception {
        super(listView);
        this.listView = listView;
    }

    @Override
    public void handleShowTimeline() {
        if (task == null) {
            task = new AllAboutMeTask(listView.getItems());
        }
        try {

            task.fillTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }
    }

}
