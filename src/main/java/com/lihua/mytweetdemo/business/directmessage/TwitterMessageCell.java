
package com.lihua.mytweetdemo.business.directmessage;

import com.lihua.mytweetdemo.business.TwitterEngine;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * This class create a twitterMessage cell to populate each cell of listview of
 * the direct messages history
 *
 * @author Lihua
 */
public class TwitterMessageCell extends ListCell<TwitterMessage> {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterMessageCell.class);
    private final TwitterEngine twitterEngine;
    Twitter twitter;

    public TwitterMessageCell() {
        this.twitterEngine = new TwitterEngine();
        this.twitter = twitterEngine.getTwitterinstance();
    }

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterMessage item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {

            try {
                setGraphic(getTwitterMessageCell(item));
            } catch (TwitterException ex) {
                LOG.debug(ex.getMessage());
            }
        }
    }

    /**
     * This method determines what the cell will look like.In this cell it will
     * show user's profile image and the text message sent and received by user
     * the user's message will have default background and the client message
     * will have light cyan background, the text message will contain send date
     * and time
     *
     * @param msg this is a TwitterMessage type which contains the info of both
     * message and user
     * @return The node to be placed into the ListView
     */
    private Node getTwitterMessageCell(TwitterMessage msg) throws TwitterException {
        HBox messageBox = new HBox();
        messageBox.setPrefSize(350, 65);
        VBox node = new VBox();
        node.setSpacing(10);

        Image image = new Image(msg.getMessageUserImageUrl(), 48, 48, true, false);
        ImageView picture = new ImageView(image);

        Date Date = msg.getMessageDate();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Text msgDate = new Text(dateFormat.format(Date));

        Text name = new Text(msg.getMessageUserName());

        Text text = new Text(msg.getMessageText());

        text.setWrappingWidth(340);

        VBox vbox = new VBox();
        if (msg.getMessageSenderId() == msg.getMessageUserId()) {
            vbox.getChildren().addAll(picture, name);
        } else {
            text.setTextAlignment(TextAlignment.RIGHT);
            node.setStyle("-fx-background-color:LIGHTCYAN");
            node.setAlignment(Pos.CENTER_RIGHT);
        }
        node.getChildren().addAll(msgDate, text);
        messageBox.getChildren().addAll(vbox, node);

        return messageBox;
    }

}
