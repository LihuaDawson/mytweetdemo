/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.directmessage;

import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.presentation.Constants;
import com.lihua.mytweetdemo.interfaces.TwitterMessageInfo;
import java.util.Date;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.TwitterException;

/**
 * this class implement TwitterMessageInfo it override all the methods of
 * TwitterMessageInfo and meanwhile relate to the user of the message and
 * retrieve user info
 *
 * @author Lihua
 */
public class TwitterMessage implements TwitterMessageInfo {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterMessage.class);

    private final TwitterUser user;
    private final DirectMessage msg;

    public TwitterMessage(DirectMessage msg, TwitterUser user) {
        this.msg = msg;
        this.user = user;

    }

    @Override
    public long getMessageUserId() {
        return user.getUserId();
    }

    @Override
    public String getMessageUserImageUrl() {

        return user.getUserImageUrl();

    }

    @Override
    public String getMessageUserName() {
        return user.getUserName();
    }

    @Override
    public Date getMessageDate() {
        return msg.getCreatedAt();
    }

    @Override
    public long getMessageId() {
        return msg.getId();
    }

    @Override
    public long getMessageRecipient() {
        return msg.getRecipientId();
    }

    @Override
    public long getMessageSenderId() {
        return msg.getSenderId();
    }

    @Override
    public String getMessageText() {

        return msg.getText();
    }

    public ResponseList<DirectMessage> handleDirectMsg() throws TwitterException {

        ResponseList<DirectMessage> sentMsg = twitter.getDirectMessages(20);
        return sentMsg;

    }

}
