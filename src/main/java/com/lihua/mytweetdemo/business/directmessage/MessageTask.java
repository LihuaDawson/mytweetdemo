/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.directmessage;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class will filter the messages get by user and according to the
 * recipient user chosen, it will only show the relevant message of user and
 * recipient
 *
 * @author Lihua
 */
public class MessageTask {

    private final static Logger LOG = LoggerFactory.getLogger(MessageTask.class);
    private final ObservableList<TwitterMessage> msgList;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private final Twitter twitter = twitterEngine.getTwitterinstance();

    public MessageTask(ObservableList<TwitterMessage> msgList) {
        this.msgList = msgList;
    }

    /**
     * This method first load all the direct messages of client then according
     * to which user has been chosen on the interface it will select only the
     * messages sent and received between this user and client, and assign these
     * message to the observableList msgList
     *
     * @param user this is the TwitterUser client choose on the interface
     * @throws TwitterException
     */
    public void fillUserDirectMessage(TwitterUser user) throws TwitterException {
        System.out.println("userid: " + twitter.getId() + "username: " + twitter.getScreenName() + "user name:" + user.getUserName());
        String cursor = null;
        int count = 20;
        int msgcount = 0;
        DirectMessageList messages;
        msgList.clear();
        do {

            System.out.println("cursor: " + cursor);
            messages = cursor == null ? twitter.getDirectMessages(count) : twitter.getDirectMessages(count, cursor);
            for (DirectMessage message : messages) {

                if ((message.getRecipientId() == user.getUserId() && message.getSenderId() == twitter.getId())
                        || (message.getRecipientId() == twitter.getId() && message.getSenderId() == user.getUserId())) {
                    msgList.add(msgList.size(), new TwitterMessage(message, user));
                    msgcount++;

                }
            }
            cursor = messages.getNextCursor();
        } while (messages.size() > 0 && cursor != null);
    }

}
