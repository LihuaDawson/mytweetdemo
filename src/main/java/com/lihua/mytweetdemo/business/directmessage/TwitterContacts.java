/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.directmessage;

import com.lihua.mytweetdemo.business.TwitterUser;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * This class create to retrieve all the contacts
 *
 * @author Lihua
 */
public class TwitterContacts {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterContacts.class);
    private final Twitter twitter;
    private final ObservableList<TwitterUser> list;
    private long cursor = -1L;
    private PagableResponseList<User> friends;

    public TwitterContacts(ObservableList<TwitterUser> list) {
        this.list = list;

        this.twitter = TwitterFactory.getSingleton();
    }

    /**
     * this method use screen name to get all the friends list and convert the
     * user list to TwitterUser list
     *
     * @param screenName
     * @throws TwitterException
     */
    public void getTwitterContacts(String screenName) throws TwitterException {
        do {
            friends = twitter.getFollowersList(screenName, cursor);
            for (User friend : friends) {
                list.add(list.size(), new TwitterUser(friend));
            }

        } while ((cursor = friends.getNextCursor()) != 0);

    }

}
