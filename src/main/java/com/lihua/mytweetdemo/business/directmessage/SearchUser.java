/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.directmessage;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.business.TwitterUserCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * this class is create to let client search a particular as a recipient to send
 * direct message
 *
 * @author Lihua
 */
public class SearchUser {

    private final static Logger LOG = LoggerFactory.getLogger(SearchUser.class);
    private final TwitterEngine twitterEngine;
    private final Twitter twitter;
    private final ListView<TwitterUser> listView;
    private final ObservableList<TwitterUser> list;

    public SearchUser(ListView<TwitterUser> listView) {
        this.twitterEngine = new TwitterEngine();
        this.twitter = twitterEngine.getTwitterinstance();
        this.listView = listView;
        this.list = FXCollections.observableArrayList();

    }

    /**
     *
     * this method will search a user according to the input string
     *
     * @param query
     */
    private void userResults(String query) {
        ResponseList<User> rawResult = lookForUser(query);
        rawResult.forEach((result)
                -> {
            list.add(list.size(), new TwitterUser(result));

        });
    }

    /**
     * this method will return a list of user who are qualified to the key word
     *
     * @param query
     * @return
     */
    private ResponseList<User> lookForUser(String query) {
        ResponseList<User> users = null;
        try {
            int page = 1;

            do {
                users = twitter.searchUsers(query, page);
                for (User user : users) {
                    if (user.getStatus() != null) {
                        System.out.println("@" + user.getScreenName() + user.getStatus().getText());

                    } else {
                        System.out.println("@" + user.getScreenName());
                    }
                }
                page++;
            } while (!users.isEmpty() && page < 50);

        } catch (TwitterException ex) {
            LOG.error("unable to search user: ", ex.getMessage());
        }
        return users;
    }

    /**
     * this method will put all the list in a VBox and return it to the
     * controller
     *
     * @param query
     * @return
     * @throws TwitterException
     */
    public Node getVBoxReseachView(String query) throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);

        listView.setItems(list);

        listView.setPrefWidth(200);

        listView.setCellFactory(p -> new TwitterUserCell());

        node.getChildren().addAll(listView);
        userResults(query);

        return node;
    }

}
