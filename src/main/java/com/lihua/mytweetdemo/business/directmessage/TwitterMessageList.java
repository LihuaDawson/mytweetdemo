package com.lihua.mytweetdemo.business.directmessage;


import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.business.TwitterUserCell;
import com.lihua.mytweetdemo.presentation.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class create a contact list and define the GUI of it, and user can click
 * any of the contact to start a direct message and view the direct messages
 * history of this user
 *
 * @author Lihua
 */
public class TwitterMessageList {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterMessageList.class);

    private final ListView<TwitterUser> listView;
    private final ObservableList<TwitterUser> list;
    private TwitterContacts twitterContacts;
    private MessageTask messageTask;
    public String recipient;
    public final ListView<TwitterMessage> msgListView;

    private final ObservableList<TwitterMessage> msgList;

    public TwitterMessageList(ListView<TwitterUser> listView, ListView<TwitterMessage> msgListView) {

        this.listView = listView;
        this.list = FXCollections.observableArrayList();
        this.msgListView = msgListView;
        this.msgList = FXCollections.observableArrayList();
    }

    /**
     * this method define a GUI for the Contact listView
     *
     * @return
     * @throws TwitterException
     */
    public Node getVBoContactView() throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);

        listView.setItems(list);

        listView.setPrefWidth(200);

        listView.setCellFactory(p -> new TwitterUserCell());

        node.getChildren().addAll(listView);

        handleShowContacts();

        return node;
    }

    /**
     * This method define the GUI of this listView
     *
     * @param sender
     * @return
     * @throws TwitterException
     */
    public Node getVBoxMsgView(TwitterUser sender) throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);

        msgListView.setItems(msgList);
        msgListView.setPrefWidth(380);
        msgListView.setCellFactory(p -> new TwitterMessageCell());

        node.getChildren().addAll(msgListView);
        handleShowMessages(sender);

        return node;

    }

    /**
     * this method is used to populate all the contacts of client to the contact
     * list
     */
    private void handleShowContacts() {

        if (twitterContacts == null) {
            LOG.info("handleShowContacts step 1");

            twitterContacts = new TwitterContacts(listView.getItems());
        }

        try {
            LOG.info("handleShowContacts step 2");
            twitterContacts.getTwitterContacts(Constants.USERSCREENNAME);
        } catch (TwitterException ex) {
            LOG.error("Unable to display timeline", ex);
        }

    }

    /**
     * This method is help to populate the items to the list
     *
     * @param sender is a TwitterUser which present an user who is chosen on the
     * interface
     * @throws TwitterException
     */
    private void handleShowMessages(TwitterUser sender) throws TwitterException {
        if (messageTask == null) {
            messageTask = new MessageTask(msgListView.getItems());

        }
        try {
            messageTask.fillUserDirectMessage(sender);
        } catch (TwitterException ex) {
            LOG.error("unable to display messagelists", ex);
        }

    }

}
