package com.lihua.mytweetdemo.business;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * @author Ken Fogel
 * @author Li Hua
 */
import com.lihua.mytweetdemo.presentation.Constants;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 *
 * @author Lihua
 */
public class Tweets {

    private final static Logger LOG = LoggerFactory.getLogger(Tweets.class);
    private TextArea sendTweetTextArea;
    private TwitterEngine twitterEngine;

    ;

    public Tweets() {
        this.twitterEngine = new TwitterEngine();
    }

    public void sendTweet(TextArea sendTweetTextArea) {
        this.sendTweetTextArea = sendTweetTextArea;
        try {
            setLimitChar(this.sendTweetTextArea);
            twitterEngine.createTweet(sendTweetTextArea.getText());
        } catch (TwitterException ex) {
            LOG.error("Unable to send tweet", ex);
        }
    }

    //set textArea to have a input length limit of 280 characters
    public void setLimitChar(TextArea sendTweetTextArea) {
        sendTweetTextArea.setWrapText(true);

        sendTweetTextArea.setTextFormatter(new TextFormatter<String>(
                change -> change.getControlNewText().length()
                <= Constants.MAX_CHARS ? change : null));

    }

}
