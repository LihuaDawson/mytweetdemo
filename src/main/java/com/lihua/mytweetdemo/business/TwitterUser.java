/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business;

import com.lihua.mytweetdemo.business.timeline.TimeLine;
import com.lihua.mytweetdemo.interfaces.TwitterUserInfo;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * this class override all the methods in the TwitterUserInfo which represent a
 * twitter user
 *
 * @author Lihua
 */
public class TwitterUser implements TwitterUserInfo {

    private final static Logger LOG = LoggerFactory.getLogger(TimeLine.class);

    private final User user;

    public TwitterUser(User user) {
        this.user = user;
    }

    public String getUserDescription() {
        return user.getDescription();
    }

    public String getUserEmail() {
        return user.getEmail();
    }

    public int getUserFriendsCount() {
        return user.getFriendsCount();
    }

    public int getFollowerCount() {
        return user.getFollowersCount();
    }

    public String getBiggerImageUrl() {
        return user.getBiggerProfileImageURL();
    }

    public Date getCreateAt() {
        return user.getCreatedAt();
    }

    @Override
    public long getUserId() {
        return user.getId();
    }

    public String getUserLocation() {
        return user.getLocation();
    }

    @Override
    public String getUserName() {
        return user.getName();
    }

    @Override
    public String getUserImageUrl() {
        return user.getProfileImageURL();
    }

    @Override
    public String getUserScreenName() {
        return user.getScreenName();
    }

    @Override
    public Status getUserStatus() {
        return user.getStatus();
    }

    public int getUserStatusCount() {
        return user.getStatusesCount();
    }

    public ResponseList<DirectMessage> handleDirectMsg() throws TwitterException {

        ResponseList<DirectMessage> sentMsg = twitter.getDirectMessages(20);
        return sentMsg;

    }

    @Override
    public DirectMessageList getMessages() throws TwitterException {
        return twitter.getDirectMessages(20);
    }
}
