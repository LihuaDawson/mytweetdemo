/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * @author Ken Fogel
 */
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterEngine {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TwitterEngine.class);

    public Twitter getTwitterinstance() {
        /**
         * if not using properties file, we can set access token by following
         * way
         */
//	ConfigurationBuilder cb = new ConfigurationBuilder();
//	cb.setDebugEnabled(true)
//	  .setOAuthConsumerKey("//TODO")
//	  .setOAuthConsumerSecret("//TODO")
//	  .setOAuthAccessToken("//TODO")
//	  .setOAuthAccessTokenSecret("//TODO");
//	TwitterFactory tf = new TwitterFactory(cb.build());
//	Twitter twitter = tf.getSingleton();

        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }

    /**
     * Send a tweet
     *
     * @param tweet
     * @return
     * @throws TwitterException
     */
    public String createTweet(String tweet) throws TwitterException {
        LOG.debug("createTweet: " + tweet);
        Twitter twitter = getTwitterinstance();
        Status status = twitter.updateStatus(tweet);
        return status.getText();
    }

    /**
     * Get the timeline
     *
     * @return
     * @throws TwitterException
     */
    public List<Status> getTimeLine(int page) throws TwitterException {
        LOG.debug("getTimeLine");
        Twitter twitter = getTwitterinstance();
        Paging paging = new Paging();
        paging.setCount(20);
        paging.setPage(page);
        List<Status> statuses = twitter.getHomeTimeline(paging);
        return statuses;
//        return statuses.stream().map(
//                item -> item.getText()).collect(
//                        Collectors.toList());
    }

    /**
     * Send direct message
     *
     * @param recipientName//recipient is a screenname which start by@
     * @param msg
     * @return
     * @throws TwitterException
     */
    public String sendDirectMessage(String recipientName, String msg) throws TwitterException {
        LOG.debug("sendDirectMessage");
        Twitter twitter = getTwitterinstance();
        DirectMessage message = twitter.sendDirectMessage(recipientName, msg);
        return message.getText();
    }

    /**
     * Search for tweets with specific contents
     *
     * @param searchTerm
     * @return
     * @throws TwitterException
     */
    public List<Status> searchtweets(String searchTerm) throws TwitterException {
        LOG.debug("searchtweets");
        Twitter twitter = getTwitterinstance();
        Query query = new Query(searchTerm);
        //Query query = new Query("source:twitter4j baeldung");
        QueryResult result = twitter.search(query);
        List<Status> statuses = result.getTweets();
        return statuses;
    }

    /**
     *
     * @param timeLine
     */
    public void displayTimeLine(List<Status> timeLine) {
        System.out.println("Length of timeline: " + timeLine.size());
        timeLine.stream().map((s) -> {
            System.out.println("User: " + s.getUser().getName());
            return s;
        }).forEachOrdered((s) -> {
            System.out.println("Text: " + s.getSource());
        });
    }

}
