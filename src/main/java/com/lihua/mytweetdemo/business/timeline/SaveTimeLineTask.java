
package com.lihua.mytweetdemo.business.timeline;


import com.lihua.mytweetdemo.persistance.TimeLineDao;
import com.lihua.mytweetdemo.persistance.TimeLineDaoImpl;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  This class create a task for object TimeLineBean
 * to retrieve user's saved timeline from database
 * @author Lihua
 */
public class SaveTimeLineTask implements TwitterTimelineTask {

    private final static Logger LOG = LoggerFactory.getLogger(SaveTimeLineTask.class);
    private final ObservableList<TwitterTimeLineInfo> list;

    private int page;

    public SaveTimeLineTask(ObservableList<TwitterTimeLineInfo> list) {

        this.list = list;
        this.page = 1;
    }

    /**
     * This method will retrieve all timelines saved in database
     * and populate them into list of TimeLineBean
     * @throws Exception 
     */
    public void fillTimeLine() throws Exception {

        TimeLineDao timelineDAO = new TimeLineDaoImpl();
        List<TimeLineBean> timelines = timelineDAO.findAll();

        timelines.forEach((timeline) -> {

            list.add(list.size(), new TimeLineBean(timeline.getTweetId(), timeline.getName(),
                    timeline.getScreenName(), timeline.getImageURL(), timeline.getSource(), timeline.getText(), timeline.getCreateAt(),
                    timeline.isRetweetedByMe(), timeline.getRetweetCount(), timeline.isFavorited(), timeline.getFavoriteCount()));

        });

        page += 1;
    }

}
