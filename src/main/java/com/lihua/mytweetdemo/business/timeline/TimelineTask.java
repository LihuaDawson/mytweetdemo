/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * Task of retrieving user's timeline
 *
 * @author tomo
 */
public class TimelineTask implements TwitterTimelineTask{
    // Real programmers use logging, not System.out.println

    private final static Logger LOG = LoggerFactory.getLogger(TimelineTask.class);

    private final ObservableList<TwitterTimeLineInfo> list;

    private final TwitterEngine twitterEngine;

    private int page;

    /**
     * Non-default constructor initializes instance variables.
     *
     * @param list
     */
    public TimelineTask(ObservableList<TwitterTimeLineInfo> list) {
        twitterEngine = new TwitterEngine();
        this.list = list;
        this.page = 1;
    }

    /**
     * Add new Status objects to the ObservableList. Additions occur at the end
     * of the list.
     *
     * @throws Exception
     */
    @Override
    public void fillTimeLine() throws Exception {

        List<Status> timeline = twitterEngine.getTimeLine(page);

        timeline.forEach((status) -> {

            list.add(list.size(), new TimeLine(status));

        });

        page += 1;
    }
    
   

}
