
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class will put a list of timelineBean into 
 * a listView and put this listView into an container
 * @author Lihua
 */
public class SavedTimeLineList extends TimelineList{
private final static Logger LOG  = LoggerFactory.getLogger(SavedTimeLineList.class);
    private TwitterTimelineTask task;
    ListView<TwitterTimeLineInfo> listView;
    
    
    public SavedTimeLineList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception {
        super(listView);
        this.listView =listView;
    }
    

    @Override
        public void handleShowTimeline(){
        
      if(task==null)
      {
          task = new SaveTimeLineTask(listView.getItems());
      }
          
     try{
                      
          task.fillTimeLine();
        }
      catch(Exception ex){
         LOG.error("Unable to display timeline", ex);
      }
        
    }

}





    

