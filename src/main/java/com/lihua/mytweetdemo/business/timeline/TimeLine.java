
package com.lihua.mytweetdemo.business.timeline;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.sql.Timestamp;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * Twitter information for ListCell
 * This class defines which members of the Status object you wish to display.
 * 
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 * 
 * @author tomo
 */
public class TimeLine implements TwitterTimeLineInfo{

    private final static Logger LOG = LoggerFactory.getLogger(TimeLine.class);
    private final Status status;

    public TimeLine(Status status) {
        this.status = status;
        
    }
    public String getUrl(){
        return status.getSource();
    }
  
    //return username who send this tweet
    @Override
    public String getName() {
        return status.getUser().getName();
    }
    //return text of this tweet
    @Override
    public String getText(){
        return status.getText();
    }
    //return the image url of the user who send this tweet
    @Override
    public String getImageURL(){
        return status.getUser().getProfileImageURL();
    }
    //return user's screen name who send this tweet
    @Override
    public String getScreenName() {
      return status.getUser().getScreenName();
    }

    
    public void retweetStatus(long tweetId) throws TwitterException{
        twitter.retweetStatus(tweetId);
    }
  //return the number of how many people like this tweet
    @Override
    public int getFavoriteCount(){
        return status.getFavoriteCount();
    }
   
    @Override
    public boolean isFavorited(){
        return status.isFavorited();
    }
    //return the date when this tweet is created
   @Override
    public Date getCreateAt(){
        
        return status.getCreatedAt();
    }
    
    //returns the tweeet object of the original tweet that was quoted

    public Status getQuotedStatus(){
        return status.getQuotedStatus();
    }
    //returns the id of the original tweet that was quoted
    public long getQuotedStatusId(){
        return status.getQuotedStatusId();
    }
    //return the number of how many times this tweet has been retweeted
    @Override
    public int getRetweetCount(){
        return status.getRetweetCount();
    }
    //return the tweet of this tweet
 
    public Status getRetweetdStatus(){
        return status.getRetweetedStatus();
    }
    //return the source of this tweet
    @Override
    public String getSource(){
        return status.getSource();
    }
    //return the user of this tweet
   
    public User getUser(){
        return status.getUser();
    }
    
    //test if the status is retweeted
   
    public boolean isRetweeted(){
        return status.isRetweet();
    }
    
    //return true if the authenticating user has retweeted this tweet , 
    //or false when the tweet was created before this feature was enabled
  
    @Override
    public boolean isRetweetedByMe(){
        return status.isRetweetedByMe();
    }
    @Override
    public long getTweetId(){
        return status.getId();
    }

    @Override
    public void setName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setText(String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setImageURL(String url) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setScreenName(String screenname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setFavoriteCount(int favoriCount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setFavorited(boolean isFavorite) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 

    @Override
    public void setRetweetCount(int retweetCount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSource(String source) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTweetId(long tweetid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCreateAt(Timestamp createAt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setRetweetedByMe(boolean isRetweetedByMe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
   

