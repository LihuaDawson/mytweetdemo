
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.io.IOException;
import java.sql.SQLException;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class create a popup window for user to reply a timeline
 *
 * @author Lihua
 */
public class ReplyPopup {

    private final static Logger LOG = LoggerFactory.getLogger(ReplyPopup.class);
    TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();

    /**
     * this method define and display a popup window
     *
     * @param timeline
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public void display(TwitterTimeLineInfo timeline) throws SQLException, IOException {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);

        VBox layout = new VBox(10);
        layout.setPrefSize(398, 400);
        TimeLineCell replyCell = new TimeLineCell();
        VBox replyBox = (VBox) replyCell.getTwitterInfoCell(timeline);

        TextField replyText = new TextField();
        replyText.setPrefSize(350, 200);
        Button replyBtn = new Button();
        replyBtn.setText("Reply");
        replyBtn.setPrefSize(100, 40);

        replyBtn.setOnAction(eh -> {

            try {

                String username = timeline.getScreenName();
                StatusUpdate latestStatus = new StatusUpdate("Replying to@" + username + '\n' + replyText.getText());
               
                twitter.updateStatus(latestStatus);

                replyText.clear();
                popupwindow.close();
            } catch (TwitterException ex) {
                LOG.error("the status not updated");
            }
        });

        layout.getChildren().addAll(replyBox, replyText, replyBtn);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout, 600, 400);
        scene.getStylesheets().add("/styles/Styles.css");
        popupwindow.setScene(scene);

        popupwindow.showAndWait();

    }
}
