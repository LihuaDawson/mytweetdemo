
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * This class implements TwitterTimeLineInfo interface
 * @author Lihua
 */
public class TimeLineBean implements TwitterTimeLineInfo{
    
    private int id;
    private long tweetId;
    private String name;
    private String screenName;
    private String imageUrl;
    private String source;    
    private String text;  
    private Timestamp createAt; 
    private boolean isRetweetedByMe;
    private int retweetCount;   
    private boolean isFavorite;  
    private int favoriteCount;  
 
    public TimeLineBean(){
        super();
    }
  

    @Override
    public String toString() {
        return "TimeLineBean{" + "id=" + id + ", tweetId=" + tweetId + ", name=" + name + ", screenName=" + screenName + ", imageUrl=" + imageUrl + ", source=" + source + ", text=" + text + ", createAt=" + createAt + ", isRetweetedByMe=" + isRetweetedByMe + ", retweetCount=" + retweetCount + ", isFavorite=" + isFavorite + ", favoriteCount=" + favoriteCount + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.id;
        hash = 41 * hash + (int) (this.tweetId ^ (this.tweetId >>> 32));
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.screenName);
        hash = 41 * hash + Objects.hashCode(this.imageUrl);
        hash = 41 * hash + Objects.hashCode(this.source);
        hash = 41 * hash + Objects.hashCode(this.text);
        hash = 41 * hash + Objects.hashCode(this.createAt);
        hash = 41 * hash + (this.isRetweetedByMe ? 1 : 0);
        hash = 41 * hash + this.retweetCount;
        hash = 41 * hash + (this.isFavorite ? 1 : 0);
        hash = 41 * hash + this.favoriteCount;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimeLineBean other = (TimeLineBean) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.tweetId != other.tweetId) {
            return false;
        }
        if (this.isRetweetedByMe != other.isRetweetedByMe) {
            return false;
        }
        if (this.retweetCount != other.retweetCount) {
            return false;
        }
        if (this.isFavorite != other.isFavorite) {
            return false;
        }
        if (this.favoriteCount != other.favoriteCount) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.screenName, other.screenName)) {
            return false;
        }
        if (!Objects.equals(this.imageUrl, other.imageUrl)) {
            return false;
        }
        if (!Objects.equals(this.source, other.source)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.createAt, other.createAt)) {
            return false;
        }
        return true;
    }

    public TimeLineBean( long tweetId, String name, String screenName, String imageUrl, 
            String source, String text, Timestamp createAt, boolean isRetweetedByMe, int retweetCount, boolean isFavorite, int favoriteCount) {
        
        this.tweetId = tweetId;
        this.name = name;
        this.screenName = screenName;
        this.imageUrl = imageUrl;
        this.source = source;
        this.text = text;
        this.createAt = createAt;
        this.isRetweetedByMe = isRetweetedByMe;
        this.retweetCount = retweetCount;
        this.isFavorite = isFavorite;
        this.favoriteCount = favoriteCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  @Override
    public long getTweetId() {
        return tweetId;
    }

    @Override
    public void setTweetId(long tweetId) {
        this.tweetId = tweetId;
    }

  @Override
    public String getName() {
        return name;
    }

  @Override
    public void setName(String name) {
        this.name = name;
    }

  @Override
    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

 
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

  @Override
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

  @Override
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

  @Override
    public Timestamp getCreateAt() {
        return createAt;
    }


  @Override
    public int getRetweetCount() {
        return retweetCount;
    }

    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

  @Override
    public boolean isFavorited() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

  @Override
    public int getFavoriteCount() {
        return favoriteCount;
    }

  @Override
    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    @Override
    public String getImageURL() {
      return imageUrl;
    }

    @Override
    public void setImageURL(String url) {
        this.imageUrl = url;
    }

    @Override
    public void setFavorited(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }


  @Override
    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }
    
  @Override
  public boolean isRetweetedByMe(){
      return isRetweetedByMe;
  }
  
  @Override
  public void setRetweetedByMe(boolean isRetweetedByMe){
      this.isRetweetedByMe = isRetweetedByMe;
  }
   
  

}