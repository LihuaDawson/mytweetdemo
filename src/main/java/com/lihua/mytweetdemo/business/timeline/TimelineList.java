/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

    /**
     *This class is used to fill in timeline list
     * @author Lihua
     */
    public class TimelineList implements TwitterTimelineList{
        private final static Logger LOG  = LoggerFactory.getLogger(TimelineList.class);
        private final ListView<TwitterTimeLineInfo> listView ;
        private  TwitterTimelineTask task;
        private final ObservableList<TwitterTimeLineInfo>list;
     
       public TimelineList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception{
       
        this.listView = listView;
        this.list =  FXCollections.observableArrayList();   
    }
    @Override
    public Node getVBoxView(){
        VBox node = new VBox();
        node.setPadding(new Insets(10,10,10,10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        
        list.clear();//TO test if is it the place to initialize the list,everytime clear the list then add 20 to it
      
         
        listView.setItems(list);
              
        listView.setPrefWidth(600);
              
        listView.setCellFactory(p->new TimeLineCell());
               
        node.getChildren().addAll(listView);
                
           // Event handler when a cell is selected
        listView.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends TwitterTimeLineInfo> ov, TwitterTimeLineInfo t, TwitterTimeLineInfo t1) -> {
                    if (t != null) {
                        LOG.info("Previous handle: " + t.getScreenName());
                    }
                    if (t1 != null) {
                        LOG.info("New handle: " + t1.getScreenName());
                    }
                });
             handleShowTimeline();
            

        return node;
    }
    
 /**
  * this method will retrieve data into list
  */
    @Override
    public void handleShowTimeline(){
        
      if(task==null)
      {
          task = new TimelineTask(listView.getItems());
      }
          
     try{
                      
          task.fillTimeLine();
        }
      catch(Exception ex){
         LOG.error("Unable to display timeline", ex);
      }
        
    }
    
}
