
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.io.IOException;
import java.sql.SQLException;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class create a popup for user to retweet a timeline with comment
 *
 * @author Lihua
 */
public class RetweetPopup {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetPopup.class);
    TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();

    /**
     * define and display this popup window
     *
     * @param timeline
     */
    public void display(TwitterTimeLineInfo timeline) throws SQLException, IOException {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Comment this tweet");
        VBox layout = new VBox(10);
        layout.setPrefSize(398, 400);
        TimeLineCell reTweet = new TimeLineCell();
        VBox retweetBox = (VBox) reTweet.getTwitterInfoCell(timeline);
        retweetBox.setPrefSize(350, 100);
        TextField inputComment = new TextField();
        inputComment.setPrefSize(350, 200);
        Button sendBtn = new Button();
        ImageView sendIcon = new ImageView("/icons/send.png");
        sendIcon.setFitWidth(20);
        sendIcon.setFitHeight(20);
        sendBtn.setPrefSize(30, 30);
        sendBtn.setGraphic(sendIcon);

        sendBtn.setOnAction(eh -> {

            try {

                String username = timeline.getScreenName();
                String tweetid = Long.toString(timeline.getTweetId());
                String url = inputComment.getText() + " https://twitter.com/" + username + "/status/" + tweetid;

                twitter.updateStatus(url);
                inputComment.clear();
                popupwindow.close();
            } catch (TwitterException ex) {
                LOG.error("the status not updated");
            }
        });

        layout.getChildren().addAll(retweetBox, inputComment, sendBtn);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout, 600, 400);
        scene.getStylesheets().add("/styles/Styles.css");
        popupwindow.setScene(scene);

        popupwindow.showAndWait();

    }

}
