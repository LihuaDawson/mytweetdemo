
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.persistance.TimeLineDao;
import com.lihua.mytweetdemo.persistance.TimeLineDaoImpl;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * ListCell for TimeLine This class represents the contents of an HBox that
 * contains tweet info
 *
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 *
 * @author tomo
 * @author Ken Fogel
 * @author Li Hua
 */
public class TimeLineCell extends ListCell<TwitterTimeLineInfo> {

    private final static Logger LOG = LoggerFactory.getLogger(TimeLineCell.class);
    private final TwitterEngine twitterEngine;
    Twitter twitter;
    public Button replyBtn;
    public Button retweetBtn;
    public Button likeBtn;
    public Button bookMarkBtn;

    public TimeLineCell() {
        this.twitterEngine = new TwitterEngine();
        this.twitter = twitterEngine.getTwitterinstance();
    }

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterTimeLineInfo item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {

            try {
                setGraphic(getTwitterInfoCell(item));
            } catch (SQLException ex) {
                LOG.debug("cannot update item", ex.getMessage());
            } catch (IOException ex) {
                LOG.debug("cannot find database property");
            }
        }
    }

    /**
     * This method determines what the cell will look like. The cell will
     * contains a user's photo, name and the create date and the text of the
     * timeline, put all this info into a VBox and return it
     *
     * @param info
     * @return The node to be placed into the ListView
     */
    public Node getTwitterInfoCell(TwitterTimeLineInfo info) throws SQLException, IOException {

        VBox node = new VBox();
        node.setSpacing(10);

        Image image = new Image(info.getImageURL(), 48, 48, true, false);
        ImageView imageView = new ImageView(image);

        Text name = new Text(info.getName());
        name.setWrappingWidth(100);

        Date infoTime = info.getCreateAt();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Text timeLineDate = new Text(dateFormat.format(infoTime));

        Text text = new Text(info.getText());
        text.setWrappingWidth(380);

        VBox vbox = new VBox();
        HBox buttonsBar;
        buttonsBar = (HBox) getHBoxButtons(info);
        vbox.getChildren().addAll(name, timeLineDate, text);
        node.getChildren().addAll(imageView, vbox, buttonsBar);

        return node;
    }

    /**
     * this method will return a HBox which contains three buttons in a button
     * bar
     *
     * @param info
     * @return
     */
    private Node getHBoxButtons(TwitterTimeLineInfo info) throws SQLException, IOException {

        HBox node = new HBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setPrefHeight(40);
        node.setPrefWidth(200);
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        likeBtn = getLikeBtn(info);
        replyBtn = getReplyBtn(info);
        retweetBtn = getRetweetBtn(info);
        bookMarkBtn = getSaveTweetBtn(info);
        node.getChildren().addAll(replyBtn, retweetBtn, likeBtn, bookMarkBtn);
        return node;
    }
    /**
     * This method will return a button for user to click
     * either save the tweet or delete the tweet
     * @param info
     * @return
     * @throws SQLException
     * @throws IOException 
     */
    private Button getSaveTweetBtn(TwitterTimeLineInfo info) throws SQLException, IOException {
        bookMarkBtn = new Button();
        bookMarkBtn.setPrefSize(80, 40);
        final ImageView savedIcon =new ImageView("icons/saved.png");
        savedIcon.setFitHeight(20);
            savedIcon.setFitWidth(20);
        final ImageView unsavedIcon = new ImageView("icons/unsaved.png");
         unsavedIcon.setFitHeight(20);
            unsavedIcon.setFitWidth(20);
        TimeLineDaoImpl timelineDao = new TimeLineDaoImpl();
        if (timelineDao.isAlreadySaved(info.getTweetId())) {
            Tooltip delete = new Tooltip("delete");
            bookMarkBtn.setTooltip(delete);  
            bookMarkBtn.setGraphic(savedIcon);

        } else {
            Tooltip save = new Tooltip("save");
            bookMarkBtn.setTooltip(save);
          
           
            bookMarkBtn.setGraphic(unsavedIcon);

        }

        bookMarkBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent ActionEvent) {
                try {
                    
                    if (timelineDao.isAlreadySaved(info.getTweetId())) {
                        
                        bookMarkBtn.setGraphic(unsavedIcon);
                        deleteSavedTimeLine(info);
                        
                    } else {
                        bookMarkBtn.setGraphic(savedIcon);
                        saveTimeLine(info);
                        
                    }
                } catch (SQLException ex) {
                    LOG.debug("cannot create bookemarkBtn", ex.getMessage());
                }
            }
        });

        return bookMarkBtn;
    }
    /**
     * This method will delete the tweet which is already 
     * saved in the database and switch icon
     * @param info 
     */
    private void deleteSavedTimeLine(TwitterTimeLineInfo info) {

                TimeLineDao timelineDao = null;
                try {
                    timelineDao = new TimeLineDaoImpl();
                } catch (IOException ex) {
                    LOG.debug("cannot get database property");
                }
                try {
                    int result = timelineDao.deleteTimeLine(info.getTweetId());
                    if (result != 1) {
                        LOG.debug("the delete method is wrong, the delete item number is not equal to 1");
                    }
                } catch (SQLException ex) {
                    LOG.debug("fail to delete a timeline in database", ex.getMessage());

                }
            }
     
    
    /**
     * This method save one tweet to an object TimeLineBean
     * for storing in database and switch icon
     * @param info 
     */
    private void saveTimeLine(TwitterTimeLineInfo info) {

                TwitterTimeLineInfo timeLine = new TimeLineBean();

                timeLine.setTweetId(info.getTweetId());
                timeLine.setName(info.getName());
                timeLine.setScreenName(info.getScreenName());
                timeLine.setImageURL(info.getImageURL());

                timeLine.setSource(info.getSource());
                timeLine.setText(info.getText());
                Timestamp ts = new Timestamp(info.getCreateAt().getTime());
                timeLine.setCreateAt(ts);
                timeLine.setRetweetedByMe(info.isRetweetedByMe());
                timeLine.setRetweetCount(info.getRetweetCount());
                timeLine.setFavorited(info.isFavorited());
                timeLine.setFavoriteCount(info.getFavoriteCount());
                TimeLineDao timelineDao = null;
                try {
                    timelineDao = new TimeLineDaoImpl();
                } catch (IOException ex) {
                    LOG.debug("cannot get database property");
                }
                try {
                    int itemnumber = timelineDao.create((TimeLineBean) timeLine);
                    System.out.println(Integer.toString(itemnumber));
                    if (itemnumber != 1) {
                        LOG.debug("the create method is wrong, the insert number is not equal to 1");
                    }
                } catch (SQLException ex) {
                    LOG.debug("fail to create a timeline in database", ex.getMessage());

                }
            }
        

    /**
     * this method will return a like button for user to favorite this timeline
     *
     * @param info
     * @return
     */
    private Button getLikeBtn(TwitterTimeLineInfo info) {
        likeBtn = new Button();
        likeBtn.setPrefSize(80, 40);
        Tooltip like = new Tooltip("Like");
        likeBtn.setTooltip(like);
        ImageView likeIcon = new ImageView("icons/like.png");
        likeIcon.setFitHeight(20);
        likeIcon.setFitWidth(20);
        ImageView redLikeIcon = new ImageView("icons/redLike.png");
        redLikeIcon.setFitHeight(20);
        redLikeIcon.setFitWidth(20);
        if (info.isFavorited()) {
            likeBtn.setGraphic(redLikeIcon);
        } else {
            likeBtn.setGraphic(likeIcon);
        }

        if (info.getFavoriteCount() != 0) {
            likeBtn.setText(Integer.toString(info.getFavoriteCount()));
        }

        likeBtn.setOnAction((ActionEvent) -> {
            try {
                if (info.isFavorited()) {

                    unfavorite(info, likeIcon);

                } else {

                    favorite(info, redLikeIcon);

                }
            } catch (TwitterException ex) {
                LOG.info("the like button cannot be handled", ex.getMessage());
            }
        });
        return likeBtn;
    }

    /**
     * this method will return a retweet button which will retweet this timeline
     *
     * @param info
     * @return
     */
    private Button getRetweetBtn(TwitterTimeLineInfo info) {
        retweetBtn = new Button();
        retweetBtn.setPrefSize(80, 40);
        Tooltip retweet = new Tooltip("Retweet");
        retweetBtn.setTooltip(retweet);

        showRetweetBtn(info);
        handleRetweetCount(info);
        retweetBtn.setOnAction((ActionEvent) -> {

            handleRetweet(info);
            handleRetweetCount(info);
        });
        return retweetBtn;
    }

    /**
     * this method will show different status of button looking according to if
     * user has retweeted it or not
     *
     * @param info
     */
    private void showRetweetBtn(TwitterTimeLineInfo info) {
        ImageView retweetIcon = new ImageView("icons/retweet.png");
        retweetIcon.setFitHeight(20);
        retweetIcon.setFitWidth(20);
        ImageView myRetweetIcon = new ImageView("icons/myretweet.png");
        myRetweetIcon.setFitHeight(20);
        myRetweetIcon.setFitWidth(20);
        if(info.isRetweetedByMe())
        {
            retweetBtn.setGraphic(myRetweetIcon);
        }
        else{
            retweetBtn.setGraphic(retweetIcon);
        }
    }

    /**
     * this method will show the count of how many times this timeline has been
     * retweeted
     *
     * @param info
     */
    private void handleRetweetCount(TwitterTimeLineInfo info) {

        if (info.getRetweetCount() != 0) {
            retweetBtn.setText(Integer.toString(info.getRetweetCount()));
        }
    }

    /**
     * this method will define a reply button and if click will reply to the
     * timeline user
     *
     * @param info
     * @return
     */
    private Button getReplyBtn(TwitterTimeLineInfo info)  {
        replyBtn = new Button();

        replyBtn.setPrefSize(80, 40);
        Tooltip reply = new Tooltip("Reply");
        replyBtn.setTooltip(reply);
        ImageView replyIcon = new ImageView("icons/chat.png");
        replyIcon.setFitHeight(20);
        replyIcon.setFitWidth(20);
        replyBtn.setGraphic(replyIcon);
        replyBtn.setOnAction((ActionEvent) -> {
            ReplyPopup replyWindow = new ReplyPopup();
            try {
                replyWindow.display(info);
            } catch (SQLException ex) {
                LOG.debug("cannot create pop up for reply button");
            } catch (IOException ex) {
                LOG.debug("cannot find database property");
            } 
        });
        return replyBtn;
    }

    /**
     * this method will handle retweet this timeline it will show a contextmenu
     * and provide with two option one is retweet directly another is retweet
     * with comment, and for the second one it will triger a popup window to let
     * user input comments
     *
     * @param info
     */
    private void handleRetweet(TwitterTimeLineInfo info) {

        ContextMenu retweetMenu = new ContextMenu();
        retweetMenu.setOnShowing((WindowEvent e) -> {
            LOG.info("retweetmenu showing");
        });

        MenuItem retweetItem = new MenuItem("Retweet");
        retweetItem.setOnAction((ActionEvent e) -> {
            LOG.info("retweet here");
            try {

//                info.retweetStatus(info.getRetweetId()); //only show updates in profile not in timeline
                String username = info.getScreenName();
                String tweetid = Long.toString(info.getTweetId());
                String url = "https://twitter.com/" + username + "/status/" + tweetid;
                twitter.updateStatus(url);
            } catch (TwitterException ex) {
                LOG.info(ex.getErrorMessage());
            }
        });

        MenuItem retweetWithCommentItem = new MenuItem("Retweet with comment");
        retweetWithCommentItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                LOG.info("retweet with comment here");
                RetweetPopup pop = new RetweetPopup();
                try {
                    pop.display(info);
                } catch (SQLException ex) {
                    LOG.debug("can not create pop for retweet");
                } catch (IOException ex) {
                    LOG.debug("cannot find database property");
                }
            }
        });
        retweetMenu.getItems().addAll(retweetItem, retweetWithCommentItem);

        retweetBtn.setContextMenu(retweetMenu);
        retweetMenu.show(retweetBtn, Side.BOTTOM, 0, 0);
    }

    /**
     * this method handle cancel like relationship
     *
     * @param info
     * @param likeIcon
     * @throws TwitterException
     */
    private void unfavorite(TwitterTimeLineInfo info, ImageView likeIcon) throws TwitterException {
        twitter.destroyFavorite(info.getTweetId());
        likeBtn.setGraphic(likeIcon);
        likeBtn.setText(Integer.toString(info.getFavoriteCount() - 1));

        LOG.info("destroy Favorite");
    }

    /**
     * this method will create like relationship
     *
     * @param info
     * @param redLikeIcon
     * @throws TwitterException
     */
    private void favorite(TwitterTimeLineInfo info, ImageView redLikeIcon) throws TwitterException {
        twitter.createFavorite(info.getTweetId());

        likeBtn.setGraphic(redLikeIcon);

        likeBtn.setText(Integer.toString(info.getFavoriteCount() + 1));

        LOG.info("create favorite");
    }

}
