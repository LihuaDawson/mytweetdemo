
package com.lihua.mytweetdemo.business.timeline;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class is created to show all the pagable results by search key word
 *
 * @author Lihua
 */
public class SearchTweet {

    private final static Logger LOG = LoggerFactory.getLogger(SearchTweet.class);
    private final TwitterEngine twitterEngine;
    private final Twitter twitter;
    private final ListView<TwitterTimeLineInfo> listView;
    private final ObservableList<TwitterTimeLineInfo> list;
    private final int page;

    /**
     * constructor receive int page which is represent the times of a button
     * click which is in the controller each time the button clicked, the page
     * number will be plus one and the list will be turn over
     *
     * @param listView
     * @param page
     */
    public SearchTweet(ListView<TwitterTimeLineInfo> listView, int page) {
        twitterEngine = new TwitterEngine();
        twitter = twitterEngine.getTwitterinstance();
        this.listView = listView;
        this.list = FXCollections.observableArrayList();
        this.page = page;
    }

    /**
     * this method receive a string which represents a key word user wants to
     * search, this method will populate certain number of results into a list
     *
     * @param query
     * @param page
     */
    private void tweetResults(String query, int page) {
        List<Status> rawResult = lookForTweet(query, page);
        rawResult.forEach((result)
                -> {
            list.add(list.size(), new TimeLine(result));

        });
    }

    /**
     * this method search for tweets contains a key word and return a certain
     * number of list
     *
     * @param queryWords
     * @param page
     * @return
     */
    private List<Status> lookForTweet(String queryWords, int page) {
        List<Status> tweets = null;

        try {
            int count = (page - 1) * 20;
            Query query = new Query(queryWords);
            QueryResult result;
            do {

                result = twitter.search(query);
                tweets = result.getTweets();
                for (Status tweet : tweets) {

                    System.out.println("@" + tweet.getUser().getScreenName() + tweet.getText());
                    count++;
                }

            } while ((query = result.nextQuery()) != null && count % 20 != 0);

        } catch (TwitterException ex) {
            LOG.error("unable to search tweet: ", ex.getMessage());
        }

        return tweets;
    }

    /**
     * this method create GUI for the list of results and return this VBox to
     * controller
     *
     * @param query
     * @param page
     * @return
     * @throws TwitterException
     */
    public Node getTweetsVBoxView(String query, int page) throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setPrefSize(600, 500);
        node.setAlignment(Pos.CENTER);

        listView.setItems(list);

        listView.setPrefWidth(500);

        listView.setCellFactory(p -> new TimeLineCell());

        node.getChildren().addAll(listView);
        tweetResults(query, page);

        return node;
    }

}
