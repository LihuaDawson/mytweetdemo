/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class will create GUI for TwitterUser it defines each cell of each
 * twitteruser
 *
 * @author Lihua
 */
public class TwitterUserCell extends ListCell<TwitterUser> {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterUserCell.class);
    private Button followBtn;
    private Button sendMsgBtn;

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterUser item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {

            setGraphic(getTwitterUserCell(item));
        }
    }

    /**
     * This method determines what the cell will look like. Here is where you
     * can add buttons or any additional information
     *
     * @param info
     * @return The node to be placed into the ListView
     */
    private Node getTwitterUserCell(TwitterUser user) {

        HBox node = new HBox();
        node.setSpacing(10);

        Image image = new Image(user.getUserImageUrl(), 48, 48, true, false);
        ImageView imageView = new ImageView(image);

        Text name = new Text(user.getUserName());
        name.setWrappingWidth(120);

        Text screenName = new Text(user.getUserScreenName());
        screenName.setWrappingWidth(120);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(name, screenName);
        node.getChildren().addAll(imageView, vbox);

        return node;
    }

    /**
     * this method define the GUI of a buttons bar and will be called by the
     * controller
     *
     * @param user
     * @return
     */
    private Node getHBoxButtons(TwitterUser user) {
        LOG.info("in getHBoxbutton bar");
        HBox node = new HBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setPrefHeight(40);
        node.setPrefWidth(200);
        node.setSpacing(50);
        node.setAlignment(Pos.CENTER);
        followBtn = new Button();
        followBtn.setPrefSize(80, 30);
        followBtn.setText("Follow");
        followBtn.setStyle("-fx-font-size:14");
        followBtn.setWrapText(false);
        sendMsgBtn = new Button();
        sendMsgBtn.setPrefSize(30, 30);

        ImageView sendMsgIcon = new ImageView("icons/newmessage.png");
        sendMsgIcon.setFitHeight(20);
        sendMsgIcon.setFitWidth(20);
        sendMsgBtn.setGraphic(sendMsgIcon);

        sendMsgBtn.setOnAction((ActionEvent) -> {
            handleDirectMsg(user);
        });

        node.getChildren().addAll(followBtn, sendMsgBtn);

        return node;
    }

    /**
     * this method will create a VBox to show direct message interface
     *
     * @param user
     */
    private void handleDirectMsg(TwitterUser user) {
        VBox directMsgBox = new VBox(2);
        directMsgBox.setPrefWidth(398);
        directMsgBox.setPrefHeight(600);
        Label recipient = new Label();
        recipient.setPrefSize(398, 60);
        recipient.setText(user.getUserScreenName());

        directMsgBox.getChildren().addAll(recipient);

    }

}
