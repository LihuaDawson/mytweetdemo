
package com.lihua.mytweetdemo.business.properties;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 *  
 * @author Ken Fogel
 */
public class PropertyManager {
       /**
     * Updates a TweetConfigBean object with the contents of the properties file
     *
     * @param twitterConfig
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final TwitterConfigProperties twitterConfig, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties oauth = new Properties();

       Path  txtFile = Paths.get(path,propFileName + ".properties");
     
        System.out.println("path : "+txtFile.getFileName().toString());
        // File must exist
        if (Files.exists(txtFile)) {
            
            try (InputStream propFileStream = newInputStream(txtFile);) {
                oauth.load(propFileStream);
            }
            twitterConfig.setConsumerKey(oauth.getProperty("oauth.consumerKey"));
            twitterConfig.setConsumerSecret(oauth.getProperty("oauth.consumerSecret"));
            twitterConfig.setAccessToken(oauth.getProperty("oauth.accessToken"));
            twitterConfig.setAccessTokenSecret(oauth.getProperty("oauth.accessTokenSecret"));

            found = true;
        }
     
        return found;
    }

     /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param twitterConfig The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final TwitterConfigProperties twitterConfig) throws IOException {

        Properties oauth = new Properties();

        oauth.setProperty("oauth.consumerKey", twitterConfig.getConsumerKey());
        oauth.setProperty("oauth.consumerSecret", twitterConfig.getConsumerSecret());
        oauth.setProperty("oauth.accessToken", twitterConfig.getAccessToken());
        oauth.setProperty("oauth.accessTokenSecret", twitterConfig.getAccessTokenSecret());
        Path txtFile = Paths.get(path+ propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            oauth.store(propFileStream, "twitter4j");
        }
    }
}
