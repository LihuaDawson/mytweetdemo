package com.lihua.mytweetdemo.business.properties;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 *
 * @author Lihua
 */
public class DatabasePropertyManager {

    public final String getUser(final DatabaseProperty database, final String path, final String propFileName) throws IOException {

        Properties databaseProperty = new Properties();

        Path txtFile = Paths.get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {

            try (InputStream propFileStream = newInputStream(txtFile);) {
                databaseProperty.load(propFileStream);
            }
        }

        return databaseProperty.getProperty("user");

    }

    public final String getPassword(final DatabaseProperty database, final String path, final String propFileName) throws IOException {

        Properties databaseProperty = new Properties();

        Path txtFile = Paths.get(path, propFileName + ".properties");

        if (Files.exists(txtFile)) {

            try (InputStream propFileStream = newInputStream(txtFile);) {
                databaseProperty.load(propFileStream);
            }
        }

        return databaseProperty.getProperty("password");

    }

    public final String getUrl(final DatabaseProperty database, final String path, final String propFileName) throws IOException {

        Properties databaseProperty = new Properties();

        Path txtFile = Paths.get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {

            try (InputStream propFileStream = newInputStream(txtFile);) {
                databaseProperty.load(propFileStream);
            }
        }

        return databaseProperty.getProperty("url");

    }

}
