
package com.lihua.mytweetdemo.business.properties;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * this class create an object to define the properties of twitter4j
 *
 * @author Lihua
 * @author Ken Fogel
 */
public class TwitterConfigProperties {

    private final StringProperty consumerKey;
    private final StringProperty consumerSecret;
    private final StringProperty accessToken;
    private final StringProperty accessTokenSecret;

    /**
     * Default Constructor
     */
    public TwitterConfigProperties() {
        this("", "", "", "");
    }

    public TwitterConfigProperties(final String consumerKey, final String consumerSecret,
            final String accessToken, final String accessTokenSecret) {
        this.consumerKey = new SimpleStringProperty(consumerKey);
        this.consumerSecret = new SimpleStringProperty(consumerSecret);
        this.accessToken = new SimpleStringProperty(accessToken);
        this.accessTokenSecret = new SimpleStringProperty(accessTokenSecret);
    }

    public String getConsumerKey() {
        return consumerKey.get();
    }

    public String getConsumerSecret() {
        return consumerSecret.get();
    }

    public String getAccessToken() {
        return accessToken.get();
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret.get();
    }

    public void setConsumerKey(String s) {
        consumerKey.set(s);
    }

    public void setConsumerSecret(String s) {
        consumerSecret.set(s);
    }

    public void setAccessToken(String s) {
        accessToken.set(s);
    }

    public void setAccessTokenSecret(String s) {
        accessTokenSecret.set(s);
    }

    public StringProperty consumerKeyProperty() {
        return consumerKey;
    }

    public StringProperty consumerSecretProperty() {
        return consumerSecret;
    }

    public StringProperty AccessTokenProperty() {
        return accessToken;
    }

    public StringProperty AccessTokenSecretProperty() {
        return accessTokenSecret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.consumerKey.get());
        hash = 17 * hash + Objects.hashCode(this.consumerSecret.get());
        hash = 17 * hash + Objects.hashCode(this.accessToken.get());
        hash = 17 * hash + Objects.hashCode(this.accessTokenSecret.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterConfigProperties other = (TwitterConfigProperties) obj;
        if (!Objects.equals(this.consumerKey.get(), other.consumerKey.get())) {
            return false;
        }
        if (!Objects.equals(this.consumerSecret.get(), other.consumerSecret.get())) {
            return false;
        }
        if (!Objects.equals(this.accessToken.get(), other.accessToken.get())) {
            return false;
        }
        return Objects.equals(this.accessTokenSecret.get(), other.accessTokenSecret.get());
    }

    @Override
    public String toString() {
        return "TwitterConfigProperties\n{\t" + "consumerKey=" + consumerKey.get()
                + "\n\tconsumerSecret=" + consumerSecret.get() + "\n\taccessToken="
                + accessToken.get() + "\n\taccessTokenSecret=" + accessTokenSecret.get() + "\n}";
    }
}
