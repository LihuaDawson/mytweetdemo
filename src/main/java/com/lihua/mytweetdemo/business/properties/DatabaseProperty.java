
package com.lihua.mytweetdemo.business.properties;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 * This class is to set database properties
 * @author Lihua
 */
public class DatabaseProperty {



    private final StringProperty password;
    private final StringProperty user;
        private final StringProperty url;



    /**
     * Default Constructor
     */
    public DatabaseProperty() {
        this("", "", "");
    }

    public DatabaseProperty(final String user, final String url,
            final String password) {
        this.user = new SimpleStringProperty(user);
        this.url = new SimpleStringProperty(url);
        this.password = new SimpleStringProperty(password);
        
    }

    public String getUser() {
        return user.get();
    }

    public String getPassword() {
        return password.get();
    }

    public String getUrl() {
        return url.get();
    }

 

    public void setUser(String s) {
        user.set(s);
    }

    public void setPassword(String s) {
        password.set(s);
    }

    public void setUrl(String s) {
        url.set(s);
    }



    public StringProperty userProperty() {
        return user;
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public StringProperty urlProperty() {
        return url;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.user.get());
        hash = 17 * hash + Objects.hashCode(this.password.get());
        hash = 17 * hash + Objects.hashCode(this.url.get());
       
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DatabaseProperty other = (DatabaseProperty) obj;
        if (!Objects.equals(this.user.get(), other.user.get())) {
            return false;
        }
        if (!Objects.equals(this.password.get(), other.password.get())) {
            return false;
        }
     
        return Objects.equals(this.url.get(), other.url.get());
    }

    @Override
    public String toString() {
        return "databaseProperty\n{\t" + "user=" + user.get()
                + "\n\tpassword=" + password.get() + "\n\turl="
                + url.get() + "\n}";
    }
}


