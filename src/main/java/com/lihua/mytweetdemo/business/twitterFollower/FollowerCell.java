/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.twitterFollower;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class put each item in the list into a cell and identify how this cell
 * will be looked like
 *
 * @author Lihua
 */
public class FollowerCell extends ListCell<TwitterUser> {

    private final static Logger LOG = LoggerFactory.getLogger(FollowerCell.class);
    private Button followBtn;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterUser item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {

            setGraphic(getFollwerCell(item));
        }
    }

    /**
     * this method identify how each follower cell will look like it will
     * contain each follower's image, username, screen name, and a button bar
     *
     * @param follower
     * @return
     */
    private Node getFollwerCell(TwitterUser follower) {
        HBox node = new HBox();
        node.setSpacing(10);

        Image image = new Image(follower.getUserImageUrl(), 48, 48, true, false);
        ImageView imageView = new ImageView(image);

        Text name = new Text(follower.getUserName());
        name.setWrappingWidth(120);

        Text screenName = new Text(follower.getUserScreenName());
        screenName.setWrappingWidth(120);

        VBox vbox = new VBox();
        HBox buttonsBar = (HBox) getHBoxButtons(follower);
        buttonsBar.setAlignment(Pos.CENTER_LEFT);
        vbox.getChildren().addAll(name, screenName);
        node.getChildren().addAll(imageView, vbox, buttonsBar);

        return node;
    }

    /**
     * this method create a button bar for each follower cell to let user to
     * choose follow or unfollow
     *
     * @param user
     * @return
     */
    public Node getHBoxButtons(TwitterUser user) {

        HBox node = new HBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setPrefHeight(40);
        node.setPrefWidth(200);
        node.setSpacing(50);
        node.setAlignment(Pos.CENTER);
        followBtn = new Button();
        followBtn.setPrefSize(120, 30);
        followBtn.setText("Following");
        followBtn.setStyle("-fx-font-size:14");
        followBtn.setWrapText(false);

        followBtn.setOnAction((ActionEvent) -> {

            try {
                if (followBtn.getText().equals("Following")) {
                    unfollowing(user);

                } else {
                    follow(user);

                }
            } catch (TwitterException ex) {
                LOG.info("cannot unfollow");
            }

        });

        node.getChildren().addAll(followBtn);

        return node;
    }

    /**
     * this method responsible to unfollow a user
     *
     * @param user
     * @throws TwitterException
     */
    public void unfollowing(TwitterUser user) throws TwitterException {
        twitter.destroyFriendship(user.getUserScreenName());
        followBtn.setText("Follow");

    }

    /**
     * this method responsible to follow a user
     *
     * @param user
     * @throws TwitterException
     */
    public void follow(TwitterUser user) throws TwitterException {
        twitter.createFriendship(user.getUserScreenName());
        followBtn.setText("Following");
    }

}
