/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.twitterFollower;

/**
 *
 * @author Lihua
 */
import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * this class create a popup window for showing profile details of the user
 *
 * @author Lihua
 */
public class profilePopup {

    private final static Logger LOG = LoggerFactory.getLogger(profilePopup.class);
    TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();
    FollowerCell follwerCell = new FollowerCell();

    /**
     * this method will set GUI of the popup window and populate details of user
     * profile data into it
     *
     * @param user
     */
    public void display(TwitterUser user) {
        Stage popupwindow = new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("This is a pop up window");
        VBox layout = new VBox(10);
        layout.setPrefSize(398, 400);

        Label name = new Label(user.getUserName());
        Label discription = new Label(user.getUserDescription());

        ImageView picture = new ImageView(user.getUserImageUrl());

        Label email = new Label(user.getUserEmail());
        int following = user.getUserFriendsCount();
        int follower = user.getFollowerCount();
        Label followerCount = new Label(following + "Following" + '\t' + follower + "Followers");
        Button messageBtn = new Button();
        messageBtn.setPrefSize(30, 30);
        ImageView sendMsgIcon = new ImageView("icons/newmessage.png");
        sendMsgIcon.setFitHeight(20);
        sendMsgIcon.setFitWidth(20);
        messageBtn.setGraphic(sendMsgIcon);
        messageBtn.setOnAction((ActionEvent) -> {

            VBox dialogueBox = handleDirectMsg(user);

            layout.getChildren().add(dialogueBox);
        });

        Button close = new Button("Close");

        close.setOnAction(e -> popupwindow.close());

        HBox followerBox = (HBox) follwerCell.getHBoxButtons(user);
        followerBox.getChildren().addAll(messageBtn, close);
        layout.getChildren().addAll(name, picture, discription, email, followerCount, followerBox);

        layout.setAlignment(Pos.CENTER);

        Scene scene1 = new Scene(layout, 600, 400);

        popupwindow.setScene(scene1);

        popupwindow.showAndWait();

    }

    /**
     * this method create a VBox and in this VBox user can sent and receive
     * direct message
     *
     * @param user
     * @return
     */
    private VBox handleDirectMsg(TwitterUser user) {
        VBox directMsgBox = new VBox(2);
        directMsgBox.setPrefWidth(398);
        directMsgBox.setPrefHeight(200);

        directMsgBox.setAlignment(Pos.BOTTOM_CENTER);

        TextArea inputMsg = new TextArea();
        Button sendBtn = new Button();
        sendBtn.setPrefSize(30, 30);
        ImageView sendIcon = new ImageView("icons/send.png");
        sendIcon.setFitHeight(20);
        sendIcon.setFitWidth(20);
        sendBtn.setGraphic(sendIcon);

        sendBtn.setOnAction((ActionEvent) -> {

            String sentMsg = inputMsg.getText();
            String recipient = user.getUserScreenName();
            if (sentMsg != null) {
                try {

                    twitterEngine.sendDirectMessage(recipient, sentMsg);
                } catch (TwitterException ex) {
                    LOG.error("unable to send message");
                }

            }

            inputMsg.clear();
        });

        directMsgBox.getChildren().addAll(inputMsg, sendBtn);
        return directMsgBox;

    }

}
