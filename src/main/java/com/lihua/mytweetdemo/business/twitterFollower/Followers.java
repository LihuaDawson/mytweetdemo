/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.twitterFollower;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.presentation.Constants;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * this class create a new datatype Followers: it is the users who follow this
 * user
 *
 * @author Lihua
 */
public class Followers {

    private final static Logger LOG = LoggerFactory.getLogger(Followers.class);
    TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();
    private PagableResponseList<User> followers;

    private final ObservableList<TwitterUser> followerList;

    long cursor = -1;
    IDs ids;

    /**
     * the constructor initialize a follower list
     *
     * @param followerList
     */
    public Followers(ObservableList<TwitterUser> followerList) {
        this.followerList = followerList;

    }

    /**
     * this method populate the content of the list
     *
     * @param screenName
     * @throws TwitterException
     */
    public void getTwitterFollowers(String screenName) throws TwitterException {
        do {
            followers = twitter.getFollowersList(Constants.USERSCREENNAME, cursor);
            for (User follower : followers) {
                followerList.add(followerList.size(), new TwitterUser(follower));
            }

        } while ((cursor = followers.getNextCursor()) != 0);
    }

}
