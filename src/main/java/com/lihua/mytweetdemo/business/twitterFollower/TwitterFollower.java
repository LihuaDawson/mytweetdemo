/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.twitterFollower;


import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.presentation.Constants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * this class try to connect user with message it will list all the followers
 * and followings and create a gui to show them and once click anyone of them,
 * will show profile details and can send direct message
 *
 * @author Lihua
 */
public class TwitterFollower {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterFollower.class);
    private final ListView<TwitterUser> followerListView;
    private final ListView<TwitterUser> followingListView;
    private final ObservableList<TwitterUser> followerList;
    private final ObservableList<TwitterUser> followingList;
    private Followers followers;
    private Following following;

    /**
     * this constructor will initialize two listView one is for followers and
     * one is for followings
     *
     * @param followerListView
     * @param followingListView
     */
    public TwitterFollower(ListView<TwitterUser> followerListView, ListView<TwitterUser> followingListView) {
        this.followerListView = followerListView;
        this.followerList = FXCollections.observableArrayList();
        this.followingListView = followingListView;
        this.followingList = FXCollections.observableArrayList();

    }

    /**
     * this method will create a GUI to show all the followers
     *
     * @return
     * @throws TwitterException
     */
    public Node getFollowersVBoxView() throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        node.setPrefSize(700, 600);

        followerListView.setItems(followerList);

        followerListView.setPrefWidth(600);

        followerListView.setCellFactory(p -> new FollowerCell());

        node.getChildren().addAll(followerListView);

        handleShowFollowers();

        return node;
    }

    /**
     * this method will create a GUI to show all the followings
     *
     * @return
     * @throws TwitterException
     */
    public Node getFollowingVBoxView() throws TwitterException {
        VBox node = new VBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        node.setPrefSize(700, 600);

        followingListView.setItems(followingList);

        followingListView.setPrefWidth(600);

        followingListView.setCellFactory(p -> new FollowerCell());

        node.getChildren().addAll(followingListView);

        handleShowFollowing();

        return node;
    }

    /**
     * this method will populate the list of following
     */
    private void handleShowFollowing() {

        if (following == null) {

            following = new Following(followingListView.getItems());
        }

        try {

            following.getTwitterFollowing(Constants.USERSCREENNAME);
        } catch (TwitterException ex) {
            LOG.error("Unable to display followers", ex);
        }

    }

    /**
     * this method will populate the list of followers
     */
    private void handleShowFollowers() {

        if (followers == null) {

            followers = new Followers(followerListView.getItems());
        }

        try {

            followers.getTwitterFollowers(Constants.USERSCREENNAME);
        } catch (TwitterException ex) {
            LOG.error("Unable to display followers", ex);
        }

    }

}
