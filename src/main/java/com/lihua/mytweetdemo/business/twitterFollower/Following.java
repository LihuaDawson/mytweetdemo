/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.business.twitterFollower;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.presentation.Constants;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * this class create a datatype Following : all the TwitterUsers who are user
 * follows.
 *
 * @author Lihua
 */
public class Following {

    private final static Logger LOG = LoggerFactory.getLogger(Following.class);
    TwitterEngine twitterEngine = new TwitterEngine();
    Twitter twitter = twitterEngine.getTwitterinstance();
    private PagableResponseList<User> following;
    private final ObservableList<TwitterUser> followingList;
    long cursor = -1;
    IDs ids;

    /**
     * initialize a following list
     *
     * @param followingList
     */
    public Following(ObservableList<TwitterUser> followingList) {

        this.followingList = followingList;

    }

    /**
     * populate following list
     *
     * @param screenName
     * @throws TwitterException
     */
    public void getTwitterFollowing(String screenName) throws TwitterException {
        do {

            following = twitter.getFriendsList(Constants.USERSCREENNAME, cursor);
            for (User follow : following) {
                followingList.add(followingList.size(), new TwitterUser(follow));
            }

        } while ((cursor = following.getNextCursor()) != 0);
    }

}
