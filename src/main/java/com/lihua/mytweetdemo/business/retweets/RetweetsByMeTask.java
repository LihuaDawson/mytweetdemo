
package com.lihua.mytweetdemo.business.retweets;


import com.lihua.mytweetdemo.business.timeline.TimeLine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * This class is used to find all the retweets by me
 * @author Lihua
 */
public class RetweetsByMeTask implements TwitterTimelineTask {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetsByMeTask.class);

    private final ObservableList<TwitterTimeLineInfo> list;

    private int page;

    /**
     * Non-default constructor initializes instance variables.
     * @param list
     */
    public RetweetsByMeTask(ObservableList<TwitterTimeLineInfo> list) {
        super();
        this.list = list;
//        this.list = list;
        this.page = 1;
    }

    /**
     * Add new Status objects to the ObservableList. Additions occur at the end
     * of the list.
     * @throws Exception
     */
    @Override
    public void fillTimeLine() throws Exception {

        List<Status> timeline = twitterEngine.getTimeLine(page);

        timeline.forEach((status) -> {
            if (status.isRetweetedByMe()) {
                list.add(list.size(), new TimeLine(status));
            }

        });
        System.out.println("REtweetby me size: "+list.size());

        page += 1;
    }

}
