
package com.lihua.mytweetdemo.business.retweets;

import com.lihua.mytweetdemo.business.timeline.TimeLine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import static com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo.twitter;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * This class is to find all the retweets of me
 * @author Lihua
 */
public class RetweetsOfMeTask implements TwitterTimelineTask {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetsOfMeTask.class);

    private final ObservableList<TwitterTimeLineInfo> list;


    /**
     * Non-default constructor initializes instance variables.
     * @param list
     */
    public RetweetsOfMeTask(ObservableList<TwitterTimeLineInfo> list) {
        super();
        this.list = list;

    }

    /**
     * Add new Status objects to the ObservableList. Additions occur at the end
     * of the list.
     * @throws Exception
     */
    @Override
    public void fillTimeLine() throws Exception {

        List<Status> timeline = twitter.getRetweetsOfMe();

        timeline.forEach((status) -> {

            list.add(list.size(), new TimeLine(status));

        });

    }

}
