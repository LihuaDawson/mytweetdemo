
package com.lihua.mytweetdemo.business.retweets;

import com.lihua.mytweetdemo.business.timeline.TimelineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.scene.control.ListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This class is used to find the retweets list of me
 * @author Lihua
 */
public class RetweetsOfMeList extends TimelineList {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetsOfMeList.class);

    private final ListView<TwitterTimeLineInfo> listView;
    private TwitterTimelineTask task;

    public RetweetsOfMeList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception {
        super(listView);
        this.listView = listView;
    }

    @Override
    public void handleShowTimeline() {
        if (task == null) {
            task = new RetweetsOfMeTask(listView.getItems());
        }
        try {

            task.fillTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }
    }

}
