package com.lihua.mytweetdemo.business.retweets;

import com.lihua.mytweetdemo.business.timeline.TimelineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineTask;
import javafx.scene.control.ListView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * this class will put a list of timeline into a listView and put this listView
 * into an container
 *
 * @author Lihua
 */
public final class RetweetsByMeList extends TimelineList {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetsByMeList.class);

    private final ListView<TwitterTimeLineInfo> listView;
    private TwitterTimelineTask task;

    public RetweetsByMeList(ListView<TwitterTimeLineInfo> listView) throws TwitterException, Exception {
        super(listView);
        this.listView = listView;
    }

    /**
     * this method will retrieve data into list
     */
    @Override
    public void handleShowTimeline() {

        if (task == null) {
            task = new RetweetsByMeTask(listView.getItems());
        }

        try {

            task.fillTimeLine();
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }

    }

}
