/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.presentation;

/**
 * this class is created for constants
 * create private constructor for preventing
 * initialization
 * @author Lihua
 */
public class Constants {
    //this is the limit of input text in twitter
    public static final int MAX_CHARS = 280;
    //this is the developer user screen name
    public static final String USERSCREENNAME = "Dawson_LH_01"; 
    
    private Constants(){}
}
