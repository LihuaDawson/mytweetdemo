/**
 * Sample Skeleton for 'followers.fxml' Controller Class
 */
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.business.twitterFollower.profilePopup;
import com.lihua.mytweetdemo.business.twitterFollower.TwitterFollower;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * this controller to handle follower and following
 *
 * @author Lihua
 */
public class FollowersController {

    private final static Logger LOG = LoggerFactory.getLogger(FollowersController.class);
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="twitterFollowerPane"
    private TabPane twitterFollowerPane; // Value injected by FXMLLoader

    @FXML // fx:id="followerPane"
    private AnchorPane followerPane; // Value injected by FXMLLoader

    @FXML // fx:id="followingPane"
    private AnchorPane followingPane; // Value injected by FXMLLoader

    /**
     * handle all the follower list each user can be clicked and will show
     * profile details and can follow and unfollow
     *
     * @throws TwitterException
     */
    public void handleFollowerList() throws TwitterException {
        ListView<TwitterUser> followers = new ListView<>();
        ListView<TwitterUser> followings = new ListView<>();
        TwitterFollower twitterFollower = new TwitterFollower(followers, followings);
        followerPane.getChildren().addAll(twitterFollower.getFollowersVBoxView());
        followingPane.getChildren().addAll(twitterFollower.getFollowingVBoxView());
        followers.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                TwitterUser follower = followers.getSelectionModel().getSelectedItem();
                LOG.info("follower: ", follower.getUserName());
                LOG.info("follower: ", follower.getUserId());
                profilePopup pop = new profilePopup();
                pop.display(follower);

            }
        });
        followings.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                TwitterUser following = followings.getSelectionModel().getSelectedItem();
                LOG.info("following: ", following.getUserName());
                LOG.info("follower: ", following.getUserId());
                profilePopup pop = new profilePopup();
                pop.display(following);

            }
        });
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert twitterFollowerPane != null : "fx:id=\"twitterFollowerPane\" was not injected: check your FXML file 'followers.fxml'.";
        assert followerPane != null : "fx:id=\"followerPane\" was not injected: check your FXML file 'followers.fxml'.";
        assert followingPane != null : "fx:id=\"followingPane\" was not injected: check your FXML file 'followers.fxml'.";

    }
}
