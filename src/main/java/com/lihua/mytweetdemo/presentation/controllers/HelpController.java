
package com.lihua.mytweetdemo.presentation.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class HelpController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="birdBtn1"
    private Button birdBtn1; // Value injected by FXMLLoader

    @FXML // fx:id="birdView1"
    private ImageView birdView1; // Value injected by FXMLLoader

    @FXML // fx:id="homeBtn"
    private Button homeBtn; // Value injected by FXMLLoader

    @FXML // fx:id="homeView"
    private ImageView homeView; // Value injected by FXMLLoader

    @FXML // fx:id="notificationBtn"
    private Button notificationBtn; // Value injected by FXMLLoader

    @FXML // fx:id="notificationView"
    private ImageView notificationView; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML // fx:id="exploreView"
    private ImageView exploreView; // Value injected by FXMLLoader

    @FXML // fx:id="messageBtn"
    private Button messageBtn; // Value injected by FXMLLoader

    @FXML // fx:id="messageView"
    private ImageView messageView; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarkBtn"
    private Button bookmarkBtn; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarkView"
    private ImageView bookmarkView; // Value injected by FXMLLoader

    @FXML // fx:id="retweetsBtn"
    private Button retweetsBtn; // Value injected by FXMLLoader

    @FXML // fx:id="listView"
    private ImageView listView; // Value injected by FXMLLoader

    @FXML // fx:id="followersBtn"
    private Button followersBtn; // Value injected by FXMLLoader

    @FXML // fx:id="followersView"
    private ImageView followersView; // Value injected by FXMLLoader

    @FXML // fx:id="helpBtn"
    private Button helpBtn; // Value injected by FXMLLoader

    @FXML // fx:id="helpView"
    private ImageView helpView; // Value injected by FXMLLoader

    @FXML
    void handleBookmark(ActionEvent event) {

    }

    @FXML
    void handleFollowers(ActionEvent event) {

    }

    @FXML
    void handleHelp(ActionEvent event) {

    }

    @FXML
    void handleHome(ActionEvent event) {

    }

    @FXML
    void handleMessage(ActionEvent event) {

    }

    @FXML
    void handleNotification(ActionEvent event) {

    }

    @FXML
    void handleRetweets(ActionEvent event) {

    }

    @FXML
    void handleSearch(ActionEvent event) {

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert birdBtn1 != null : "fx:id=\"birdBtn1\" was not injected: check your FXML file 'help.fxml'.";
        assert birdView1 != null : "fx:id=\"birdView1\" was not injected: check your FXML file 'help.fxml'.";
        assert homeBtn != null : "fx:id=\"homeBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert homeView != null : "fx:id=\"homeView\" was not injected: check your FXML file 'help.fxml'.";
        assert notificationBtn != null : "fx:id=\"notificationBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert notificationView != null : "fx:id=\"notificationView\" was not injected: check your FXML file 'help.fxml'.";
        assert searchBtn != null : "fx:id=\"searchBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert exploreView != null : "fx:id=\"exploreView\" was not injected: check your FXML file 'help.fxml'.";
        assert messageBtn != null : "fx:id=\"messageBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert messageView != null : "fx:id=\"messageView\" was not injected: check your FXML file 'help.fxml'.";
        assert bookmarkBtn != null : "fx:id=\"bookmarkBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert bookmarkView != null : "fx:id=\"bookmarkView\" was not injected: check your FXML file 'help.fxml'.";
        assert retweetsBtn != null : "fx:id=\"retweetsBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert listView != null : "fx:id=\"listView\" was not injected: check your FXML file 'help.fxml'.";
        assert followersBtn != null : "fx:id=\"followersBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert followersView != null : "fx:id=\"followersView\" was not injected: check your FXML file 'help.fxml'.";
        assert helpBtn != null : "fx:id=\"helpBtn\" was not injected: check your FXML file 'help.fxml'.";
        assert helpView != null : "fx:id=\"helpView\" was not injected: check your FXML file 'help.fxml'.";

    }
}
