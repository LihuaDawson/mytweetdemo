/**
 * Sample Skeleton for 'notification.fxml' Controller Class
 */

package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.notification.AllAboutMeList;
import com.lihua.mytweetdemo.business.notification.MentionList;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
/**
 * this controller is responsible to show all the user time lines
 * and the mentioned time lines
 * @author Lihua
 */
public class NotificationController {
    private final static Logger LOG = LoggerFactory.getLogger(NotificationController.class);
    private AllAboutMeList allAboutMeList;
    ListView<TwitterTimeLineInfo> searchUserTimeline = new ListView<>();
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="notificationVbox"
    private VBox notificationVbox; // Value injected by FXMLLoader

    @FXML // fx:id="allTab"
    private Tab allTab; // Value injected by FXMLLoader

    @FXML // fx:id="allPane"
    private AnchorPane allPane; // Value injected by FXMLLoader

    @FXML // fx:id="mentionTab"
    private Tab mentionTab; // Value injected by FXMLLoader

    @FXML // fx:id="mentionsPane"
    private AnchorPane mentionsPane; // Value injected by FXMLLoader
    
        @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader


       @FXML
    void handleAllNext(ActionEvent event) throws TwitterException {
        searchUserTimeline.setItems(null);
        showAll();
        

    }
    public void showAll() throws TwitterException{
          allPane.getChildren().clear();
        
        VBox userTimelineBox = (VBox) allAboutMeList.getVBoxView();
        allPane.getChildren().addAll(userTimelineBox);
        
    }
    
    public void showMention() throws TwitterException, Exception {
         ListView<TwitterTimeLineInfo> searchMentions = new ListView<>();
        MentionList mentions = new MentionList(searchMentions);
        VBox mentionsVbox = (VBox) mentions.getVBoxView();
        mentionsPane.getChildren().addAll(mentionsVbox);

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws Exception {
        allAboutMeList = new AllAboutMeList(searchUserTimeline);
        assert notificationVbox != null : "fx:id=\"notificationVbox\" was not injected: check your FXML file 'notification.fxml'.";
        assert allTab != null : "fx:id=\"allTab\" was not injected: check your FXML file 'notification.fxml'.";
        assert allPane != null : "fx:id=\"allPane\" was not injected: check your FXML file 'notification.fxml'.";
        assert mentionTab != null : "fx:id=\"mentionTab\" was not injected: check your FXML file 'notification.fxml'.";
        assert mentionsPane != null : "fx:id=\"mentionsPane\" was not injected: check your FXML file 'notification.fxml'.";

    }
}
