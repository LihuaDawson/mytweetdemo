/**
 * Sample Skeleton for 'bookmark.fxml' Controller Class
 */

package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.timeline.SavedTimeLineList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookmarkController {

    private final static Logger LOG = LoggerFactory.getLogger(BookmarkController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="notificationVbox"
    private VBox bookmarkVbox; // Value injected by FXMLLoader

    @FXML // fx:id="nextBtn"
    private Button updateBtn; // Value injected by FXMLLoader

    @FXML // fx:id="savedTimeLinesVbox"
    private VBox savedTimeLinesVbox; // Value injected by FXMLLoader

    @FXML
    void handleUpdate(ActionEvent event) throws Exception {
        showBookmarkVbox();

    }
    public void showBookmarkVbox() throws Exception{
    savedTimeLinesVbox.getChildren().clear();
    ListView<TwitterTimeLineInfo> timelines = new ListView<>();
    SavedTimeLineList timelinelist = new SavedTimeLineList(timelines);
    VBox timelineVbox = (VBox) timelinelist.getVBoxView();
    savedTimeLinesVbox.getChildren().add(timelineVbox);
    
    
    }
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert bookmarkVbox != null : "fx:id=\"bookmarkVbox\" was not injected: check your FXML file 'bookmark.fxml'.";
        assert updateBtn != null : "fx:id=\"updateBtn\" was not injected: check your FXML file 'bookmark.fxml'.";
        assert savedTimeLinesVbox != null : "fx:id=\"savedTimeLinesVbox\" was not injected: check your FXML file 'bookmark.fxml'.";

    }
}
