
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.TwitterEngine;
import com.lihua.mytweetdemo.business.TwitterUser;
import com.lihua.mytweetdemo.business.directmessage.SearchUser;
import com.lihua.mytweetdemo.business.directmessage.TwitterMessage;
import com.lihua.mytweetdemo.business.directmessage.TwitterMessageList;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * this controller is responsible to show direct messages
 *
 * @author Lihua
 */
public class MsgController {

    private final static Logger LOG = LoggerFactory.getLogger(MsgController.class);
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private String recipient;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="msgHbox"
    private HBox msgHbox; // Value injected by FXMLLoader

    @FXML // fx:id="contactVBox"
    private VBox contactVbox; // Value injected by FXMLLoader

    @FXML // fx:id="messageIconBtn"
    private Button messageIconBtn; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML // fx:id="searchContact"
    private TextField searchContact; // Value injected by FXMLLoader

    @FXML // fx:id="receiverText"
    public Text receiverText; // Value injected by FXMLLoader

    @FXML // fx:id="sendMsgTextArea"
    private TextArea sendMsgTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="sendMsgBtn"
    private Button sendMsgBtn; // Value injected by FXMLLoader

    @FXML // fx: id = "directMsgBox"
    private VBox directMsgVbox; // Value injected by FXMLLoader

    /**
     * this method is for user to search recipient to send a direct message
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleSearchUser(ActionEvent event) throws TwitterException {

        String query = searchContact.getText();
        ListView<TwitterUser> searUsers = new ListView<>();
        SearchUser userResult = new SearchUser(searUsers);
        contactVbox.getChildren().clear();
        contactVbox.getChildren().add(userResult.getVBoxReseachView(query));

    }

    @FXML // fx:id="sentMsgVbox"
    private VBox sentMsgVbox; // Value injected by FXMLLoader

    /**
     * this method will show all the contacts of the user in a VBox, and user
     * can click any contact to view all the direct messages history and send
     * direct message
     *
     * @throws TwitterException
     */
    void handleContactVbox() throws TwitterException {

        ListView<TwitterUser> contacts = new ListView<>();
        
        ListView<TwitterMessage> sendMsg = new ListView<>();
        

        TwitterMessageList friends = new TwitterMessageList(contacts, sendMsg);
        contactVbox.getChildren().clear();
        contactVbox.getChildren().add(friends.getVBoContactView());
        contacts.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                sentMsgVbox.getChildren().clear();
                TwitterUser sender = contacts.getSelectionModel().getSelectedItem();
                recipient = sender.getUserScreenName();
                
                VBox msgList = null;
                try {
                    msgList = (VBox) friends.getVBoxMsgView(sender);
                } catch (TwitterException ex) {
                    LOG.debug("unable to show friend's messages", ex);
                }
             
                sentMsgVbox.getChildren().addAll(msgList);
                receiverText.setText("TO :   " + sender.getUserScreenName());

            }
        });

    }
    

    /**
     * this method is to handle send button, one it is clicked the message will
     * be sent
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleSendMsg(ActionEvent event) throws TwitterException {
        String inputMsg = sendMsgTextArea.getText();

        if (inputMsg != null) {
            try {
                twitterEngine.sendDirectMessage(recipient, inputMsg);
            } catch (TwitterException ex) {
                
                LOG.error("Unable to send message", inputMsg);

            }
            sendMsgTextArea.clear();
            handleContactVbox();
            
           
        }
    }

    @FXML
    void handleStartMessage(ActionEvent event) {

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert msgHbox != null : "fx:id=\"msgHbox\" was not injected: check your FXML file 'msg.fxml'.";
        assert contactVbox != null : "fx:id=\"contactVBox\" was not injected: check your FXML file 'msg.fxml'.";
        assert messageIconBtn != null : "fx:id=\"messageIconBtn\" was not injected: check your FXML file 'msg.fxml'.";
        assert searchBtn != null : "fx:id=\"searchBtn\" was not injected: check your FXML file 'msg.fxml'.";
        assert searchContact != null : "fx:id=\"searchContact\" was not injected: check your FXML file 'msg.fxml'.";
        assert receiverText != null : "fx:id=\"receiverText\" was not injected: check your FXML file 'msg.fxml'.";
        assert sendMsgTextArea != null : "fx:id=\"sendMsgTextArea\" was not injected: check your FXML file 'msg.fxml'.";
        assert sendMsgBtn != null : "fx:id=\"sendMsgBtn\" was not injected: check your FXML file 'msg.fxml'.";
        assert directMsgVbox != null : "fx:id=\"directMsgVBox\" was not injected: check your FXML file 'msg.fxml'.";
        assert sentMsgVbox != null : "fx:id=\"sentMsgVbox\" was not injected: check your FXML file 'msg.fxml'.";
    }
}
