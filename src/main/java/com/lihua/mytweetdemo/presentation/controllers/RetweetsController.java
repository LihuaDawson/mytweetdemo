/**
 * Sample Skeleton for 'retweets.fxml' Controller Class
 */

package com.lihua.mytweetdemo.presentation.controllers;


import com.lihua.mytweetdemo.business.retweets.RetweetsOfMeList;
import com.lihua.mytweetdemo.business.retweets.RetweetsByMeList;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class RetweetsController {
    private final static Logger LOG = LoggerFactory.getLogger(RetweetsController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="retweetsVbox"
    private VBox retweetsVbox; // Value injected by FXMLLoader

    @FXML // fx:id="RetweetsByMeTab"
    private Tab RetweetsByMeTab; // Value injected by FXMLLoader

    @FXML // fx:id="allPane"
    private AnchorPane retweetByMePane; // Value injected by FXMLLoader

    @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader

    @FXML // fx:id="retweetsOfMeTab"
    private Tab retweetsOfMeTab; // Value injected by FXMLLoader

    @FXML // fx:id="RetweetOfMePane"
    private AnchorPane RetweetOfMePane; // Value injected by FXMLLoader

    @FXML
    void handleAllNext(ActionEvent event) {

    }
    
    public void showRetweetsByMe() throws Exception{
//        retweetByMePane.getChildren().clear();
         ListView<TwitterTimeLineInfo> retweetsByMeList = new ListView<>();
        RetweetsByMeList retweetsByMe = new RetweetsByMeList(retweetsByMeList);
        VBox retweetsByMeBox = (VBox) retweetsByMe.getVBoxView();
        retweetByMePane.getChildren().addAll(retweetsByMeBox);
         
    }
    
    public void showRetweetsOfMe() throws TwitterException, Exception{
//        RetweetOfMePane.getChildren().clear();
        ListView<TwitterTimeLineInfo> retweetsOfMeList = new ListView<>();
        RetweetsOfMeList retweetsOfMe = new RetweetsOfMeList(retweetsOfMeList);
        VBox retweetsOfMeBox = (VBox) retweetsOfMe.getVBoxView();
        RetweetOfMePane.getChildren().addAll(retweetsOfMeBox);
        
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        
        
        assert retweetsVbox != null : "fx:id=\"retweetsVbox\" was not injected: check your FXML file 'retweets.fxml'.";
        assert RetweetsByMeTab != null : "fx:id=\"RetweetsByMeTab\" was not injected: check your FXML file 'retweets.fxml'.";
        assert retweetByMePane != null : "fx:id=\"retweetByMePane\" was not injected: check your FXML file 'retweets.fxml'.";
        assert nextBtn != null : "fx:id=\"nextBtn\" was not injected: check your FXML file 'retweets.fxml'.";
        assert retweetsOfMeTab != null : "fx:id=\"retweetsOfMeTab\" was not injected: check your FXML file 'retweets.fxml'.";
        assert RetweetOfMePane != null : "fx:id=\"RetweetOfMePane\" was not injected: check your FXML file 'retweets.fxml'.";

    }
}
