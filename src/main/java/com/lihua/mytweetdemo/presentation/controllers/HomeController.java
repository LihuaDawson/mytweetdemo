/**
 * Sample Skeleton for 'home.fxml' Controller Class
 */
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.Tweets;
import com.lihua.mytweetdemo.business.timeline.TimelineList;

import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import com.lihua.mytweetdemo.interfaces.TwitterTimelineList;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class HomeController {

    private final static Logger LOG = LoggerFactory.getLogger(HomeController.class);
    ListView<TwitterTimeLineInfo> timeLine;
    TwitterTimelineList twitterTimeLine;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="homeVbox"
    private VBox homeVbox; // Value injected by FXMLLoader

    @FXML // fx:id="tweetMsgTextArea"
    private TextArea tweetMsgTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="tweetBtn1"
    private Button tweetBtn1; // Value injected by FXMLLoader
    @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader

//    @FXML // fx:id="timeLineListView"
//    private ListView<TwitterInfo> timeLineListView; // Value injected by FXMLLoader
    @FXML // fx:id="timeLineVbox"
    private VBox timelineVbox; // Value injected by FXMLLoader

    /**
     * this method is for next button when it clicked, the method will load more
     * timelines
     *
     * @param event
     * @throws Exception
     */
    @FXML
    void loadMoreTimeLine(ActionEvent event) throws Exception {
        timeLine.setItems(null);
        twitterTimeLine.handleShowTimeline();
        showTimeLine();

    }

    /**
     * this method is to handle tweet button when it clicked the tweet will be
     * sent
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleTweetBtn(ActionEvent event) throws TwitterException, Exception {
        Tweets tweets = new Tweets();
        tweets.sendTweet(tweetMsgTextArea);
        tweetMsgTextArea.clear();
        showTimeLine();

    }

    /**
     * this method is to show timeline 20 per page if loadMoreTimeLine method
     * called it will clear the current timeline and load previous 20 timelines
     *
     * @throws Exception
     */
    public void showTimeLine() throws Exception {
        timelineVbox.getChildren().clear();    
        timelineVbox.getChildren().add(twitterTimeLine.getVBoxView());
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws Exception {
        timeLine = new ListView<>();
        twitterTimeLine = new TimelineList(timeLine);
        assert homeVbox != null : "fx:id=\"homeVbox\" was not injected: check your FXML file 'home.fxml'.";
        assert tweetMsgTextArea != null : "fx:id=\"tweetMsgTextArea\" was not injected: check your FXML file 'home.fxml'.";
        assert tweetBtn1 != null : "fx:id=\"tweetBtn1\" was not injected: check your FXML file 'home.fxml'.";
//        assert timeLineListView != null : "fx:id=\"timeLineListView\" was not injected: check your FXML file 'home.fxml'.";
        assert timelineVbox != null : "fx:id=\"timeLineVbox\" was not injected: check your FXML file 'home.fxml'.";
        assert nextBtn != null : "fx:id=\"nextBtn\" was not injected: check your FXML file 'home.fxml'.";
    }
}
