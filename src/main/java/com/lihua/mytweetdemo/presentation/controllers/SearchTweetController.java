/**
 * Sample Skeleton for 'Untitled' Controller Class
 */
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.timeline.SearchTweet;
import com.lihua.mytweetdemo.business.timeline.TimeLine;
import com.lihua.mytweetdemo.interfaces.TwitterTimeLineInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
/**
 * this controller is responsible for search tweets
 * @author Lihua
 */
public class SearchTweetController {

    private final static Logger LOG = LoggerFactory.getLogger(SearchTweetController.class);
    private int page;
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="searchTweetVbox"
    private VBox searchTweetVbox; // Value injected by FXMLLoader

    @FXML // fx:id="tweetResultBox"
    private VBox tweetResultBox; // Value injected by FXMLLoader

    @FXML // fx:id="searchTweetTextField"
    private TextField searchTweetTextField; // Value injected by FXMLLoader

    @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML
    void handleMoreTweets(ActionEvent event) throws TwitterException {
        page++;
        String query = searchTweetTextField.getText();
        ListView<TwitterTimeLineInfo> searchTweets = new ListView<>();
        SearchTweet tweets = new SearchTweet(searchTweets, page);
        tweetResultBox.getChildren().clear();
        tweetResultBox.getChildren().add(tweets.getTweetsVBoxView(query, page));

    }

    @FXML
    void handleSearchTweet(ActionEvent event) throws TwitterException {
        page = 1;
        String query = searchTweetTextField.getText();
        ListView<TwitterTimeLineInfo> searchTweets = new ListView<>();
        SearchTweet tweets = new SearchTweet(searchTweets, page);

        tweetResultBox.getChildren().clear();
        tweetResultBox.getChildren().add(tweets.getTweetsVBoxView(query, page));

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert searchTweetVbox != null : "fx:id=\"searchTweetVbox\" was not injected: check your FXML file 'Untitled'.";
        assert tweetResultBox != null : "fx:id=\"tweetResultBox\" was not injected: check your FXML file 'Untitled'.";
        assert searchBtn != null : "fx:id=\"searchBtn\" was not injected: check your FXML file 'Untitled'.";
        assert nextBtn != null : "fx:id=\"nextBtn\" was not injected: check your FXML file 'Untitled'.";
    }
}
