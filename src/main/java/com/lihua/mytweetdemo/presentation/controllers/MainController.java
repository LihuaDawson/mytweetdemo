/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.TwitterEngine;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * this main controller is the root controller of all the subcontrollers,it will
 * be the first to initialize then all the subcontrollers will be initialize
 * after it controls which subcontroller to be shown
 *
 * @author Lihua
 *
 *
 */
public class MainController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MainController.class);

    private final TwitterEngine twitterEngine = new TwitterEngine();
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    private HomeController homeController;
    private MsgController msgController;
    private FollowersController followersController;
    private SearchTweetController searchTweetController;
    private NotificationController notificationController;
    private BookmarkController bookmarkController;
    private RetweetsController retweetsController;
    private HelpController helpController;
    private VBox homeVbox;
    private HBox msgHbox;
    private VBox searchTweetVbox;
    private TabPane twitterFollowerPane;
    private VBox notificationPane;
    private VBox retweetsPane;
    private VBox bookmarkVbox;
    private BorderPane mainPane;
    private ScrollPane helpPane;

    @FXML // fx:id="birdBtn"
    private Button birdBtn; // Value injected by FXMLLoader

    @FXML // fx:id="birdView"
    private ImageView birdView; // Value injected by FXMLLoader

    @FXML // fx:id="homeBtn"
    private Button homeBtn; // Value injected by FXMLLoader

    @FXML // fx:id="homeView"
    private ImageView homeView; // Value injected by FXMLLoader

    @FXML // fx:id="notificationBtn"
    private Button notificationBtn; // Value injected by FXMLLoader

    @FXML // fx:id="notificationView"
    private ImageView notificationView; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML // fx:id="exploreView"
    private ImageView exploreView; // Value injected by FXMLLoader

    @FXML // fx:id="messageBtn"
    private Button messageBtn; // Value injected by FXMLLoader

    @FXML // fx:id="messageView"
    private ImageView messageView; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarkBtn"
    private Button bookmarkBtn; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarkView"
    private ImageView bookmarkView; // Value injected by FXMLLoader

    @FXML // fx:id="listBtn"
    private Button retweetsBtn; // Value injected by FXMLLoader

    @FXML // fx:id="listView"
    private ImageView listView; // Value injected by FXMLLoader

    @FXML // fx:id="profileBtn"
    private Button followersBtn; // Value injected by FXMLLoader

    @FXML // fx:id="profileView"
    private ImageView followersView; // Value injected by FXMLLoader

    @FXML // fx:id="moreBtn"
    private Button helpBtn; // Value injected by FXMLLoader

    @FXML // fx:id="moreView"
    private ImageView moreView; // Value injected by FXMLLoader

    @FXML // fx:id="tweetBtn"
    private Button tweetBtn; // Value injected by FXMLLoader

    //TODO
    @FXML
    void handleBookmark(ActionEvent event) throws Exception {
        LOG.info("show bookmarks");
        mainPane.setCenter(bookmarkVbox);
        bookmarkController.showBookmarkVbox();
    }

    /**
     * this method will show search tweet interface
     *
     * @param event
     */
    @FXML
    void handleSearch(ActionEvent event) {
        LOG.info("showsearchTweet");
        mainPane.setCenter(searchTweetVbox);

    }

    /**
     * this method will show home timeline interface
     *
     * @param event
     */
    @FXML
    void handleHome(ActionEvent event) throws Exception {
        LOG.info("showhome");
        mainPane.setCenter(homeVbox);
        showHomePaneFirst();

    }

    @FXML
    void handleRetweets(ActionEvent event) throws TwitterException, Exception {
        LOG.info("show retweets");
        mainPane.setCenter(retweetsPane);
        retweetsController.showRetweetsByMe();
        retweetsController.showRetweetsOfMe();

    }

    /**
     * this method will show direct message interface
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleMessage(ActionEvent event) throws TwitterException {
        LOG.info("show message pane");
        mainPane.setCenter(msgHbox);

    }
//TODO

    @FXML
    void handleHelp(ActionEvent event) {
        LOG.info("show help");
        mainPane.setCenter(helpPane);

    }

    /**
     * this method will show notification interface which contains all user
     * timelines and all mention timelines
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleNotification(ActionEvent event) throws TwitterException, Exception {
        LOG.info("show notification pane");
        mainPane.setCenter(notificationPane);
        notificationController.showAll();
        notificationController.showMention();
    }

    /**
     * this method will show follower and following interface
     *
     * @param event
     * @throws TwitterException
     */
    @FXML
    void handleFollowers(ActionEvent event) throws TwitterException {
        LOG.info("show followers pane");
        mainPane.setCenter(twitterFollowerPane);
        followersController.handleFollowerList();

    }

    /**
     * this method will initialize all the subcontrollers here
     *
     * @throws TwitterException
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws TwitterException {

        createHomePane();
        createMsgPane();
        createFollowersPane();
        createSearchTweetPane();
        createNotificationPane();
        createBookmarkPane();
        createRetweetsPane();
        createHelpPane();
    }

    /**
     * this method will be called in main app to make the home interface to be
     * default shown when program launch
     *
     * @throws Exception
     */
    public void showHomePaneFirst() throws Exception {

        mainPane.setCenter(homeVbox);
        homeController.showTimeLine();

    }
    
    private void createHelpPane(){
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(ResourceBundle.getBundle("stringBundle"));
        loader.setLocation(MainController.class.getResource("/fxml/help.fxml"));
        try{
            helpPane = (ScrollPane)loader.load();
            helpController = loader.getController();
        }
        catch(IOException ex){
            LOG.error("create help pane",ex);
            Platform.exit();
        }
    }

    private void createBookmarkPane() {
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(ResourceBundle.getBundle("stringBundle"));
        loader.setLocation(MainController.class.getResource("/fxml/bookmark.fxml"));
        try {
            bookmarkVbox = (VBox) loader.load();
            bookmarkController = loader.getController();
        } catch (IOException ex) {
            LOG.error("create bookmarkVBox", ex);
            Platform.exit();
        }

    }

    /**
     * this method will create search Tweet Pane
     */
    private void createSearchTweetPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/searchTweet.fxml"));
            searchTweetVbox = (VBox) loader.load();
            searchTweetController = loader.getController();
        } catch (IOException ex) {
            LOG.error("create searchTweetVBox", ex);
            Platform.exit();
        }
    }

    /**
     * Create the followers pane
     */
    private void createFollowersPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/followers.fxml"));
            twitterFollowerPane = (TabPane) loader.load();
            followersController = loader.getController();
        } catch (IOException ex) {
            LOG.error("create followerpane", ex);
            Platform.exit();
        }
    }

    /**
     * Create the home pane
     */
    private void createHomePane() {
        try {
            //Load mainPane layout from fxml file.

            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/home.fxml"));
            homeVbox = (VBox) loader.load();
            homeController = loader.getController();
        } catch (IOException ex) {
            LOG.error("create homePane", ex);
            Platform.exit();
        }
    }

    /**
     * Create the msgPane
     */
    private void createMsgPane() throws TwitterException {
        try {
            //Load msgPane layout from fxml file.

            FXMLLoader loader = new FXMLLoader();

            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/msg.fxml"));

            msgHbox = (HBox) loader.load();

            msgController = loader.getController();
            msgController.handleContactVbox();

        } catch (IOException ex) {
            LOG.error("create msgPane", ex);
            Platform.exit();
        }
    }

    /**
     * this method will create notification pane
     */
    private void createNotificationPane() {
        try {
            //Load notificationPane layout from fxml file.

            FXMLLoader loader = new FXMLLoader();

            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/notification.fxml"));

            notificationPane = (VBox) loader.load();

            notificationController = loader.getController();

        } catch (IOException ex) {
            LOG.error("create notificationpane", ex);
            Platform.exit();
        }
    }

    private void createRetweetsPane() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("stringBundle"));
            loader.setLocation(MainController.class.getResource("/fxml/retweets.fxml"));
            retweetsPane = (VBox) loader.load();
            retweetsController = loader.getController();
        } catch (IOException ex) {
            LOG.error("create retweetsPane", ex);
            Platform.exit();
        }
    }

    /**
     * Receive a reference to the master layout pane so that events can change
     * the center of the pane
     *
     * @param mainPane
     */
    public void setMainPane(BorderPane mainPane) {
        LOG.info("mainController", "setMainpane");
        this.mainPane = mainPane;
    }

}
