/**
 * Sample Skeleton for 'Untitled' Controller Class
 *
 * @modified Li Hua
 * @author Ken Fogel
 */
package com.lihua.mytweetdemo.presentation.controllers;

import com.lihua.mytweetdemo.business.properties.TwitterConfigProperties;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this controller is for user who don't have property files
 *
 * @author Lihua
 */
public class CreatePropertyController {

    private final static Logger LOG = LoggerFactory.getLogger(CreatePropertyController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="enterPropertyVbox"
    private VBox enterPropertyVbox; // Value injected by FXMLLoader

    @FXML // fx:id="customerKeyTextField"
    private TextField customerKeyTextField; // Value injected by FXMLLoader

    @FXML // fx:id="customerSecretTextField"
    private TextField customerSecretTextField; // Value injected by FXMLLoader

    @FXML // fx:id="accessTokenTextField"
    private TextField accessTokenTextField; // Value injected by FXMLLoader

    @FXML // fx:id="accessTockenSecretTextField"
    private TextField accessTockenSecretTextField; // Value injected by FXMLLoader

    private final TwitterConfigProperties twitterConfigProperites;

    Scene scene;
    Stage stage;
    MainController mainController;

    public CreatePropertyController() {
        super();
        twitterConfigProperites = new TwitterConfigProperties();

    }

    public void setSceneStageController(Scene scene, Stage stage, MainController mainController) {
        this.scene = scene;
        this.stage = stage;
        this.mainController = mainController;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    private void initialize() {
        assert enterPropertyVbox != null : "fx:id=\"enterPropertyVbox\" was not injected: check your FXML file 'Untitled'.";
        assert customerKeyTextField != null : "fx:id=\"customerKeyTextField\" was not injected: check your FXML file 'Untitled'.";
        assert customerSecretTextField != null : "fx:id=\"customerSecretTextField\" was not injected: check your FXML file 'Untitled'.";
        assert accessTokenTextField != null : "fx:id=\"accessTokenTextField\" was not injected: check your FXML file 'Untitled'.";
        assert accessTockenSecretTextField != null : "fx:id=\"accessTockenSecretTextField\" was not injected: check your FXML file 'Untitled'.";

        Bindings.bindBidirectional(customerKeyTextField.textProperty(), twitterConfigProperites.consumerKeyProperty());
        Bindings.bindBidirectional(customerSecretTextField.textProperty(), twitterConfigProperites.consumerSecretProperty());
        Bindings.bindBidirectional(accessTokenTextField.textProperty(), twitterConfigProperites.AccessTokenProperty());
        Bindings.bindBidirectional(accessTockenSecretTextField.textProperty(), twitterConfigProperites.AccessTokenSecretProperty());
    }
}
