package com.lihua.mytweetdemo.presentation;

import com.lihua.mytweetdemo.presentation.controllers.MainController;
import com.lihua.mytweetdemo.business.properties.PropertyManager;
import com.lihua.mytweetdemo.business.properties.TwitterConfigProperties;
import com.lihua.mytweetdemo.presentation.controllers.CreatePropertyController;

import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class is the main start program will be launch firstly check validation
 * of properties if the properties file exist it will enter into main program of
 * twitter if not exist will ask user to input key and values
 *
 * @author Lihua
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    //private VBox homeVbox;
    public Stage primaryStage;
    //private HomeController homeController;

//    private BorderPane mainPane;
    private MainController mainController;

    public MainApp() {
        super();
    }

    /**
     * this method will check which scene to start with
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        //Scene mainScene = new Scene(mainPane);
        Scene mainScene = createMainScene();
        Scene propertyScene = createEnterProperty(mainScene);

        if (!checkForProperties()) {
            this.primaryStage.setScene(propertyScene);

        } else {
            this.primaryStage.setScene(mainScene);

            mainController.showHomePaneFirst();
        }

        primaryStage.setTitle("My Twitter Demo");
        primaryStage.show();
        LOG.info("Program started");

    }

    /**
     * the main scene is the scene with twitter
     *
     * @return
     * @throws IOException
     */
    public Scene createMainScene() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        loader.setResources(ResourceBundle.getBundle("stringBundle"));
        BorderPane mainPane = (BorderPane) loader.load();

        mainController = loader.getController();
        mainController.setMainPane(mainPane);
        Scene scene = new Scene(mainPane);
        scene.getStylesheets().add("/styles/Styles.css");
        return scene;
    }

    /**
     * Check if a Properties file exists and can be loaded into a bean. This
     * does not verify that the contents of the bean fields are valid.
     *
     * @return found Have we been able to load the properties?
     */
    private boolean checkForProperties() {
        boolean found = false;
        TwitterConfigProperties twitterConfigProperties = new TwitterConfigProperties();
        PropertyManager pm = new PropertyManager();

        try {
            if (pm.loadTextProperties(twitterConfigProperties, "src/main/resources/", "twitter4j")) {
                found = true;
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
        }
        return found;
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * this scene is used for people don't have local properties file and need
     * to manually input properties values
     *
     * @param mainScene
     * @return
     * @throws IOException
     */
    private Scene createEnterProperty(Scene mainScene) throws IOException {
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource("/fxml/createProperty.fxml"));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
        loader.setResources(ResourceBundle.getBundle("stringBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (VBox) loader.load();

        // Retrieve a reference to the controller so that we can pass
        // in the properties object
        CreatePropertyController controller = loader.getController();

        // Pass the references that the properties controller will need to
        // display the second scene after entering the mail config data
        controller.setSceneStageController(mainScene, primaryStage, mainController);

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        return scene;

    }

}
