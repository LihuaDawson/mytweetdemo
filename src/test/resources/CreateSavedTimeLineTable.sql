/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lihua
 * Created: Nov 7, 2019
 */

USE TIMELINEDB;

DROP TABLE IF EXISTS TIMELINETABLE;
CREATE TABLE TIMELINETABLE (
    ID int NOT NULL auto_increment PRIMARY KEY,
    TWEETID LONG NOT NULL,
    USERNAME VARCHAR(255) NOT NULL DEFAULT '',
    SCREENNAME VARCHAR(255) NOT NULL DEFAULT '',
    IMAGE VARCHAR(255),
    URL VARCHAR(255),
    TEXT VARCHAR(280),
    CREATETIME VARCHAR(255),
    RETWEETCOUNT INT NOT NULL,
    ISFAVORITE BOOLEAN,
    FAVORITECOUNT INT
) 

ENGINE=InnoDB;/* DEFAULT STORAGE ENGINE*/


