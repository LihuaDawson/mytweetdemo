
package com.lihua.mytweetdemo.testdatabase;

import com.lihua.mytweetdemo.business.properties.DatabaseProperty;
import com.lihua.mytweetdemo.business.properties.DatabasePropertyManager;
import com.lihua.mytweetdemo.business.timeline.TimeLineBean;
import com.lihua.mytweetdemo.persistance.TimeLineDao;
import com.lihua.mytweetdemo.persistance.TimeLineDaoImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 * Unit test class
 * @author Lihua
 */
@Ignore
public class TestDatabase {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestDatabase.class);

    private  String url ;
    private  String user ;
    private  String password ;

   

    @Before
    public void seedDatabase() throws IOException {
        LOG.info("@Before seeding");
           DatabaseProperty database = new DatabaseProperty();
        DatabasePropertyManager pm = new DatabasePropertyManager();
         user= pm.getUser(database, "src/main/resources/", "databaseConfig"); 
         password = pm.getPassword(database, "src/main/resources/", "databaseConfig"); 
        url = pm.getUrl(database, "src/main/resources/", "databaseConfig"); 
        final String seedDataScript = loadAsString("CreateTimeLineTableTest.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") ;
    }


    
    /**
     * This method is to test if findAll method retrieves 
     * all the items in the database
     * @throws SQLException
     * @throws IOException 
     */
    @Test(timeout = 1000)
    public void testFindAll() throws SQLException, IOException{
        
            TimeLineDao timelineDao = new TimeLineDaoImpl();
        List<TimeLineBean> timelines = timelineDao.findAll();
        assertEquals("number of items in testdatabase", 5,timelines.size());
       
    }
    
    
    /**
     * 
     * @throws SQLException
     * @throws IOException 
     */
        @Test(timeout = 1000)
    public void testCreate() throws SQLException, IOException {
        TimeLineDao timelineDao = new TimeLineDaoImpl();

        TimeLineBean timeline = new TimeLineBean();
        timeline.setId(1);
        long id = 1193401077607079930L;
        timeline.setTweetId(id);
        timeline.setName("Dawson_LMH");
        timeline.setScreenName("dawson_lmh");
        timeline.setImageURL("http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png");
        timeline.setSource("<a href=\"https://about.twitter.com/products/tweetdeck\" rel=\"nofollow\">TweetDeck</a>");
        timeline.setText("@Dawson_SP_02 test");
        timeline.setCreateAt(Timestamp.valueOf("2019-11-10 00:32:43.0"));
        timeline.setIsFavorite(false);
        timeline.setRetweetCount(0);
        timeline.setFavorited(false);
        timeline.setFavoriteCount(0);
        
    
        timelineDao.create(timeline);
        TimeLineBean another = timelineDao.findID(timeline.getId());
     
        assertNotEquals("A record was not created", another, timeline);
    }
    
    /**
     * This method is to test if create method can
     * insert another record which contains the same tweetId
     * @throws IOException
     * @throws SQLException 
     */
    @Test(timeout=1000)
    public void testCreateAlreadyExistItem() throws IOException, SQLException{
        TimeLineDao timelineDao = new TimeLineDaoImpl();
             TimeLineBean timeline = new TimeLineBean();
        timeline.setId(1);
        long id = 1193401077607079936L;
        timeline.setTweetId(id);
        timeline.setName("Dawson_LMH");
        timeline.setScreenName("dawson_lmh");
        timeline.setImageURL("http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png");
        timeline.setSource("<a href=\"https://about.twitter.com/products/tweetdeck\" rel=\"nofollow\">TweetDeck</a>");
        timeline.setText("@Dawson_SP_02 test");
        timeline.setCreateAt(Timestamp.valueOf("2019-11-10 00:32:43.0"));
        timeline.setIsFavorite(false);
        timeline.setRetweetCount(0);
        timeline.setFavorited(false);
        timeline.setFavoriteCount(0);
        
    
        int result = timelineDao.create(timeline);
       
        assertEquals("attempt to create a tweet which tweetid is already exist: ", -1, result);
        
    }
    
       /**
        * This method is to test if create method allows record to have text column
        * more than 280
        * @throws IOException
        * @throws SQLException 
        */
      @Test(timeout=1000)
      public void testNotCreateLongText () throws IOException, SQLException{
          TimeLineDao timelineDao = new TimeLineDaoImpl();
            TimeLineBean timeline = new TimeLineBean();
        timeline.setId(1);
        long id = 1193401077607079930L;
        timeline.setTweetId(id);
        timeline.setName("Dawson_LMH");
        timeline.setScreenName("dawson_lmh");
        timeline.setImageURL("http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png");
        timeline.setSource("<a href=\"https://about.twitter.com/products/tweetdeck\" rel=\"nofollow\">TweetDeck</a>");
        timeline.setText("@Dawson_SP_02 test longggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg Stringggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"
                + "testinggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
        timeline.setCreateAt(Timestamp.valueOf("2019-11-10 00:32:43.0"));
        timeline.setIsFavorite(false);
        timeline.setRetweetCount(0);
        timeline.setFavorited(false);
        timeline.setFavoriteCount(0);
         
        int result = timelineDao.create(timeline);
       
        assertEquals("cannot create items with text more than 280 chars", -1, result);
          
      }
      
      
      /**
       * This method is test when attempting to delete the record is not exist in 
       * database, it should return -1
       * @throws IOException
       * @throws SQLException 
       */
      @Test(timeout = 1000)
      public void testDeleteNotExist() throws IOException, SQLException{
          TimeLineDao timelineDao = new TimeLineDaoImpl();
          long id = 30L;
          int result = timelineDao.deleteTimeLine(id);
          assertEquals("number of deleted items", 0, result);
      }
      
      
    /**
     * This method is to test if the delete method successfully 
     * delete one record in database it should return 1
     * @throws IOException
     * @throws SQLException 
     */  
    @Test(timeout=1000)
    public void testDelete() throws IOException, SQLException {
        TimeLineDao timelineDao = new TimeLineDaoImpl();
        long id =1193401077607079936L;
        int result = timelineDao.deleteTimeLine(id);
      
        assertEquals("number of deleted items",1,result);
    }
     /**
     * This method is to test isAlreadySaved method 
     * if the tweetId of the tweet is already saved in database
     * then it should return true
     * @throws IOException
     * @throws SQLException 
     */
    @Test (timeout = 1000)
    public void testAlreadySaved() throws IOException, SQLException{
        TimeLineDaoImpl timelineDao = new TimeLineDaoImpl();
        long id = 1193401077607079936L;
        boolean result = timelineDao.isAlreadySaved(id);
        
        
        assertEquals("the tweetid already exists in database", true, result);
                
    }
    
    /**
     * This method is to test isAlreadySaved method 
     * if the tweetId of the tweet is not saved in database
     * then it should return false
     * But the reality is if I give a very close number more than 17 digits
     * it only read the first 17 digits, so if the first 17 digits are the same,
     * and after digits are the different, it will take them as the same long number
     * @throws IOException
     * @throws SQLException 
     */
    @Test (timeout = 1000)
    public void testNotAlreadySaved() throws IOException, SQLException{
         TimeLineDaoImpl timelineDao = new TimeLineDaoImpl();
        long id = 119955666777L;
        boolean result = timelineDao.isAlreadySaved(id);
        
        
        assertEquals("the tweetid already exists in database", false, result);
    }

}
