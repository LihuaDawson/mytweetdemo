
/**
 * Author:  Lihua
 * Created: Nov 7, 2019
 */

-- This script needs to run only once
DROP DATABASE IF EXISTS TIMELINEDB;
CREATE DATABASE TIMELINEDB;

USE TIMELINEDB;

DROP USER IF EXISTS lihua@localhost;
CREATE USER lihua@'localhost' IDENTIFIED  BY 'dawson';
GRANT ALL ON TIMELINEDB.* TO lihua@'localhost';

FLUSH PRIVILEGES;
