/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lihua
 * Created: Nov 9, 2019
 */

USE TIMELINEDB;

DROP TABLE IF EXISTS TIMELINETEST;

CREATE TABLE TIMELINETEST (
    ID int NOT NULL auto_increment PRIMARY KEY,
    TWEETID LONG NOT NULL UNIQUE,
    USERNAME VARCHAR(30) NOT NULL DEFAULT '',
    SCREENNAME VARCHAR(30) NOT NULL DEFAULT '',
    IMAGE VARCHAR(255),
    URL VARCHAR(255),
    TEXT VARCHAR(280),
    CREATETIME VARCHAR(255),
    ISTWEETEDBYME BOOLEAN,
    RETWEETCOUNT INT,
    ISFAVORITE BOOLEAN,
    FAVORITECOUNT INT
) ;


INSERT INTO TIMELINETEST (ID, TWEETID, USERNAME, SCREENNAME,
IMAGE,URL,TEXT,CREATETIME,ISTWEETEDBYME,RETWEETCOUNT,ISFAVORITE,FAVORITECOUNT)
VALUES
(1,1193401077607079936,"Dawson_LMH","dawson_lmh",
"http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
'<a href="https://about.twitter.com/products/tweetdeck" rel="nofollow">TweetDeck</a>',
"@Dawson_SP_02 test",
"2019-11-10 00:32:43.0",
false,
0,false,0),
(2,1193400686421123072,"Dawson_LMH","dawson_lmh",
"http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
'<a href="https://waldo2.dawsoncollege.qc.ca" rel="nofollow">MH_LINFirstTwitterApp</a>',"welcome back https://t.co/7XZNKVuMjF","2019-11-10 00:31:10.0",
false,0,false,0),
(3,1193391452488908800,"Li Hua","Dawson_LH_01",
"http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
'<a href="https://waldo2.dawsoncollege.qc.ca" rel="nofollow">tweet_LiHua</a>',"https://t.co/EJqIDHZWhX","2019-11-09 23:54:29.0",
false,0,false,1),
(4,1193355341414719488,"Dawson_SP","Dawson_SP_02",
"http://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png",
'<a href="https://mobile.twitter.com" rel="nofollow">Twitter Web App</a>',"fdsfds","2019-11-09 21:30:59.0",
false,0,false,1),
(5,1193321120583340033,"Liam Harbec","dawson_lh",
"http://pbs.twimg.com/profile_images/1188170461814939648/McuTNWRv_normal.jpg",
'<a href="https://waldo2.dawsoncollege.qc.ca" rel="nofollow">Twitter App-ology</a>',"@Dawson_CE_01 pls don't be null (even tho it will, just need to see error log)","2019-11-09 19:15:00.0",
false,0,false,0);


